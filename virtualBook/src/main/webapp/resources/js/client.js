$( document ).ready(function() {
	$('#clientlist').dataTable({
		"sScrollY": 200,
		/*
			This will enable jQuery UI theme
		*/
		"bJQueryUI": true,
		/*
			will add the pagination links
		*/
		"sPaginationType": "full_numbers"
	});
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){

		$("#mainClientForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainClientForm input[type='tel']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainClientForm input[type='email']").each(function() {
    		this.disabled = "disabled";
    	})
    }
	
});

function clearMessages(){
	$("#message").empty();
	$("#messageError").empty();
}

function loadFromList(id){
	$("#fromSearch").val(id);
	$("#actionForSearch").val("4");
	$("#mainClientForm").submit();
}

function cancelAction(){
	$("#btnDelete").css("display","none");
	$("#btnCancel").css("display","none");
	$("#btnSave").css("display","block");
	
	$('.uname').find('input:text').val('');   
	$("#mainClientForm input").each(function() {
	    this.value = "";
	 })
	$("#mainClientForm input[type='hidden']").each(function() {
		this.value = "";
	})
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){

		$("#mainClientForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainClientForm input[type='tel']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainClientForm input[type='email']").each(function() {
    		this.disabled = "disabled";
    	})
    }else{
    	$("#mainClientForm input[type='text']").each(function() {
    		 $(this).prop("disabled", false);
    	})
    	$("#mainClientForm input[type='tel']").each(function() {
    		$(this).prop("disabled", false);
    	})
    	$("#mainClientForm input[type='email']").each(function() {
    		$(this).prop("disabled", false);
    	})
    }
}