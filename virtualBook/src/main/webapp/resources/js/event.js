$( document ).ready(function() {
	$("#subTotal").prop('disabled', true);
	$("#total").prop('disabled',true);
	
	$("#contacCheckBox").change(function(){
		   if(this.checked)
			   clientAsContact(1);
		   else
			   clientAsContact(0)
		});
	
	$("#hasContractCheckBox").change(function(){
		if(this.checked){
			hasContract(1);
		}else{
			hasContract(0);
		}
	});
	
});

function clientAsContact(val){
	if(val==1){
		if($("#ddClient option:selected").text()=="--- Select ---"){
			alert("Please select a client");
			$('#contacCheckBox').prop('checked', false);
		}else{
			$("#contactName").val($("#ddClient option:selected").text());
			$("#contactName").prop('disabled', true);
			var client = $("#ddClient option:selected");
			$("#contactPhone").val($("#ddClient option:selected").attr("phone"));
			$("#contactPhone").prop('disabled', true);
		}	
	}else{
		$("#contactName").prop('disabled', false);
		$("#contactPhone").prop('disabled', false);
		$("#contactName").val("");
		$("#contactPhone").val("");
	}
}

function hasContract(val){
	
	if(val==1){
		$("#datepickerContract").css("display","block");
	}else{
		$("#datepickerContract").css("display","none");
	}
}

function addItem(){
	var item = $("#ItemCmb option:selected").text();
	var itemId = $("#ItemCmb option:selected").val();
	var count = $("#count").val();
	var unitprice = $("#unitPrice").val();
	var summaryFlag = true;
	
	if(itemId=="NONE"){
		$("#ItemError").text("please select item!");
		return false;
	}else{
		$("#ItemError").text("");
	}	
	if($.trim(count)=="" || count<1){
		$("#countError").text("please insert quantity!");
		summaryFlag = false;
	}else{
		$("#countError").text("");
	}
	if($.trim(unitprice)=="" || unitprice <1){
		$("#unitError").text("please insert Unit price!");
		summaryFlag = false;
	}else{
		$("#unitError").text("");
	}
	if(summaryFlag==false){
		return false;
	}
	
	$('#itemsTable > tbody:last').append("<tr><td class='colItem'>"+item+"</td><td class='colCount'>"+count+"</td><td class='colUnit'>"+unitprice+"</td></tr>");
	
	$("#ItemCmb option[value='"+itemId+"']").remove();
	$("#count").val("");
	$("#unitPrice").val("");
	
	var subtotal = $("#subTotal").val();
	if(subtotal == "" ){
		subtotal = 0;
	}else{
		if(isNaN(subtotal)){
			subtotal = 0;
		}else{
			subtotal = parseFloat($("#subTotal").val());
		}
	}
	var quantity = parseFloat(count);
	var price = parseFloat(unitprice);
	
	$("#subTotal").val((price*quantity)+subtotal);
	calculateTotal();
}


function calculateTotal(){
	var subtotal = $("#subTotal").val();
	var delivery = $("#delivery").val();
	var forward  = $("#forwardPayment").val();
	var tax      = $("#taxes").val();
	var total    = $("#total").val();
	
	if(subtotal == "" || subtotal == 0 ){
		subtotal = 0.0;
	}
	if(delivery == "" || delivery == 0 ){
		delivery = 0.0;
	}
	if(forward == "" || forward == 0 ){
		forward = 0.0;
	}
	if(tax == "" || tax == 0 ){
		tax = 0.0;
	}
	if(total == "" || total == 0 ){
		total = 0.0;
	}
	total = parseFloat(subtotal) + parseFloat(delivery);
	if(tax > 0){
		total = total + ((parseFloat(tax)*100)/parseFloat(total));
	}
	total = total - parseFloat(forward);
	$("#total").val(total);
}

function validateDatePicker(value){
	var date = new Date();
	var picker = new Date($(value).val());
	if(picker<date){
		alert("choose a future date");
		$("#"+value.id).val("");
	}
}

