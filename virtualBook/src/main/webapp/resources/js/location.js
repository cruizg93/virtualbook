$( document ).ready(function() {
	$('#clientlist').dataTable({
		"sScrollY": 200,
		/*
			This will enable jQuery UI theme
		*/
		"bJQueryUI": true,
		/*
			will add the pagination links
		*/
		"sPaginationType": "full_numbers"
	});
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){

		$("#mainLocationForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainLocationForm input[type='tel']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainLocationForm input[type='email']").each(function() {
    		this.disabled = "disabled";
    	})
    }
	
});

function clearMessages(){
	$("#message").empty();
	$("#messageError").empty();
}

function loadFromList(id){
	$("#fromSearch").val(id);
	$("#actionForSearch").val("4");
	$("#mainLocationForm").submit();
}

function cancelAction(){
	$("#btnDelete").css("display","none");
	$("#btnCancel").css("display","none");
	$("#btnSave").css("display","block");
	
	$('.uname').find('input:text').val('');   
	$("#mainLocationForm input").each(function() {
	    this.value = "";
	 })
	$("#mainLocationForm input[type='hidden']").each(function() {
		this.value = "";
	})
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){

		$("#mainLocationForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainLocationForm input[type='tel']").each(function() {
    		this.disabled = "disabled";
    	})
    	$("#mainLocationForm input[type='email']").each(function() {
    		this.disabled = "disabled";
    	})
    	
    }else{
    	$("#mainLocationForm input[type='text']").each(function() {
    		 $(this).prop("disabled", false);
    	})
    	$("#mainLocationForm input[type='tel']").each(function() {
    		$(this).prop("disabled", false);
    	})
    	$("#mainLocationForm input[type='email']").each(function() {
    		$(this).prop("disabled", false);
    	})
    }
}