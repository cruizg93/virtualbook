$( document ).ready(function() {
	$('#itemlist').dataTable({
		"sScrollY": 200,
		/*
			This will enable jQuery UI theme
		*/
		"bJQueryUI": true,
		/*
			will add the pagination links
		*/
		"sPaginationType": "full_numbers"
	});
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){

		$("#mainItemForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    }
	
});

function clearMessages(){
	$("#message").empty();
	$("#messageError").empty();
}

function loadFromList(id){
	$("#fromSearch").val(id);
	$("#actionForSearch").val("4");
	$("#mainItemForm").submit();
}

function cancelAction(){
	$("#btnDelete").css("display","none");
	$("#btnCancel").css("display","none");
	$("#btnSave").css("display","block");
	
	$('.uname').find('input:text').val('');   
	$("#mainItemForm input").each(function() {
	    this.value = "";
	 })
	$("#mainItemForm input[type='hidden']").each(function() {
		this.value = "";
	})
	
	if($("#status").val()=="disabled"
    	&& $("#fromSearch").val()!= 0
    	&& $("#fromSearch").val() != "0"){
		
		$("#mainItemForm input[type='text']").each(function() {
    		this.disabled = "disabled";
    	})
    }else{
    	$("#mainItemForm input[type='text']").each(function() {
    		 $(this).prop("disabled", false);
    	})
    }
}