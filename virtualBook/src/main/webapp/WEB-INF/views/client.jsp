<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="${pageContext.request.contextPath}/resources/css/dataTable_ui.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/dataTable.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
var context = '${pageContext.request.contextPath}';
</script>
<script src="${pageContext.request.contextPath}/resources/js/client.js" type="text/javascript" > </script>
<style>
	#wrapper input:not([type="checkbox"]):not([type="button"]){
		width:90%;
		padding: 10px 0px 10px 30px;
	}
	
	#wrapper p{
		padding-bottom: 0px;
	}
</style>
<form:form action="${pageContext.request.contextPath}/client/manageClient" 
		modelAttribute="clientForm" method="POST"
		id="mainClientForm"><!-- action="client/manageClient" method="POST" modelAttribute="clientForm" -->
		<div id="container_demo">
			<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
			<div id="wrapper">
				<!-- inicio -->
				<div class="container animate form" id="client" >
					<h1>${clientForm.title}</h1>
					<div id="tabs" class="tabs">
						<nav>
							<ul>			
								<li><a href="#section-1" onclick="clearMessages();" class="icon-shop"><span>Managment</span></a></li>
								<li><a href="#section-2" onclick="clearMessages();" class="icon-cup"><span>List</span></a></li>
							</ul>
						</nav>
						<div class="content">
							<section id="section-1" class="animate form">
								<div class="mediabox">
										<form action="" style="height: 581px !important;">
											<table style="width:100%;" id="tblForm">
												<tr>
													<td>
														<p>
														<form:label for="client.name" class="uname" data-icon="u"
															path="client.name">Name Client</form:label>
														<form:input id="name" path="client.name" name="client.name"
															required="none" type="text" placeholder="Name" ></form:input>
														</p>
													</td>
													<td>
														<p>
														<form:label for="client.lastName" class="uname" data-icon="u"
															path="client.lastName">Last Name Client</form:label>
														<form:input id="lastName" path="client.lastName" name="client.name"
															required="none" type="text" placeholder="Last Name" ></form:input>
														</p>
													</td>
												</tr>
												<tr>
													<td>
														<p>
														<form:label path="client.phoneNumber" class="uname" data-icon="p">Number Phone</form:label>
														<form:input path="client.phoneNumber" name="client.phoneNumber" id="txtPhoneNumber"
															required="none"  type="tel" placeholder="(###) ### ####" ></form:input>
														</p>													
													</td>
													<td>
														<p>
														<form:label path="client.email" class="uname" data-icon="e">E-mail</form:label>
														<form:input path="client.email" name="client.email" required="none"
															placeholder="mail@mail.com" type="email"></form:input>
														</p>													
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<p>
														<form:label path="client.companyName" class="uname" data-icon="u">Company Name</form:label>
														<form:input path="client.companyName" name="client.companyName"
															required="none" placeholder="Company" style="width:95% !important;"></form:input>
														</p>		
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<p class="action button">
														<form:button value="1" name="client.actionForm" id="btnSave" style="display: ${clientForm.btnSave};">SAVE</form:button>
														<form:button value="3" name="client.actionForm" id="btnDelete" style="display: ${clientForm.btnDelete};">DELETE</form:button>
														<input type="button"  value="CANCEL"  name="" id="btnCancel" onclick="cancelAction();" style="display: ${clientForm.btnCancel};"/>
														</p>
													</td>
												</tr>
											</table>
											
										<form:hidden path="fromSearch" name="fromSearch" class="uname" value="" id="fromSearch"/>
										<form:hidden path="client.id" name="client.id" class="uname" value=""  />
										<form:hidden value="" id="actionForSearch" name="client.actionForm" path="client.actionForm"/>
										<form:hidden path="client.status" name="client.status" id="status" class="uname" value=""  />
										<form:hidden path="client.state" name="client.state" id="state" class="uname" value=""/>
										</form>
								</div>
							</section>
							<section id="section-2">
								<div class="mediabox">
									<table cellpadding="0" cellspacing="0" border="0" class="display" id="clientlist">
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="lcl" items="${clientForm.clients}" varStatus="counter">
											<tr class="odd gradeA" style="cursor: pointer;" onclick="loadFromList('${lcl.id}')">    
										    	<td>${lcl.name} ${lcl.lastName}</td>
										    	<td>${lcl.companyName}</td>
										    	<td>${lcl.email}</td>
										    	<td>${lcl.phoneNumber}</td>
										    	<td>${lcl.status}</td>
										    </tr>
										</c:forEach>									
									</tbody>
									</table>
								</div>
							</section>
						</div><!-- /content -->
					</div><!-- /tabs -->
					<h2 id="message">${msg}</h2>
					<h2 id="messageError">${msgError}</h2>
				</div>
				<script src="${pageContext.request.contextPath}/resources/js/cbpFWTabs.js"></script>
				<script>
					new CBPFWTabs( document.getElementById( 'tabs' ) );
				</script>
				<!-- fin -->
			</div>
		</div>

</form:form>
<script src="${pageContext.request.contextPath}/resources/js/cbpFWTabs.js"></script>
<script>
	new CBPFWTabs( document.getElementById( 'tabs' ) );
</script>