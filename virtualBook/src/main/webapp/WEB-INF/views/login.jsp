<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<form:form action="loginNew" method="POST" modelAttribute="loginForm">
	<section>
		<div id="container_demo">
			<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
			<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
				id="tologin"></a>
			<div id="wrapper">
				<div id="login" class="animate form">
				<h1>${loginForm.title}</h1>
					<form action="" autocomplete="on">
						<p>
							<form:label for="username" class="uname" data-icon="u"
								path="usuario">
							Your email or username 
							</form:label>
							<form:input id="username" path="usuario" name="username"
								required="required" type="text"
								placeholder="myusername or mymail@mail.com"></form:input>
						</p>
						<p>
							<form:label path="password" class="youpasswd" data-icon="p">Your password</form:label>
							<form:password path="password" id="password" name="password"
								required="required" placeholder="eg. X8df!90EO"></form:password>

						</p>					
						<p class="login button">
							<form:button value="1" name="action">Login</form:button>
							<form:button value="2" name="action">dos</form:button>
						</p>
						<p class="keeplogin">
							<label for="loginkeeping">${msgError}</label>
						</p>
						<p class="change_link">
							<label>Forgot your password ?</label> <a href="#toregister" class="to_register">Remember</a>
						</p>
					</form>
				</div>

				<div id="register" class="animate form">
					<form action="" autocomplete="on">
						<h1>Sign up</h1>
						<p>
							<label for="usernamesignup" class="uname" data-icon="u">Your
								username</label> <input id="usernamesignup" name="usernamesignup"
								required="required" type="text" placeholder="mysuperusername690" />
						</p>
						<p>
							<label for="emailsignup" class="youmail" data-icon="e">
								Your email</label> <input id="emailsignup" name="emailsignup"
								required="required" type="email"
								placeholder="mysupermail@mail.com" />
						</p>
						<p>
							<label for="passwordsignup" class="youpasswd" data-icon="p">Your
								password </label> <input id="passwordsignup" name="passwordsignup"
								required="required" type="password" placeholder="eg. X8df!90EO" />
						</p>
						<p>
							<label for="passwordsignup_confirm" class="youpasswd"
								data-icon="p">Please confirm your password </label> <input
								id="passwordsignup_confirm" name="passwordsignup_confirm"
								required="required" type="password" placeholder="eg. X8df!90EO" />
						</p>
						<p class="signin button">
							<form:button value="enviar">sign up</form:button>
						</p>
						<p class="change_link">
							Already a member ? <a href="#tologin" class="to_register"> Go
								and log in </a>
						</p>
					</form>
				</div>

			</div>
		</div>
	</section>

</form:form>



