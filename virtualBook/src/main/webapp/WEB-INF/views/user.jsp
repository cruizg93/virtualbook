<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<form:form action="" method="POST" modelAttribute="user">


	<section>
		<div id="container_demo">
			<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
			<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
				id="tologin"></a>
			<div id="wrapper">
				<div id="client" class="">
					<form action="" autocomplete="on">
						<h1>USERS</h1>
						<p>
							<form:label for="user" class="uname" data-icon="u"
								path="user">User</form:label>
							<form:input id="user" path="user"
								name="user" required="required" type="text"
								placeholder="pepito"></form:input>
						</p>

						<p>
							<form:label for="password" class="uname" data-icon="u"
								path="password">Password</form:label>
							<form:input id="password" path="password"
								name="password" required="required" type="text"
								placeholder="123456"></form:input>
						</p>

						<p class="login button">
							<form:button value="enviar">SAVE</form:button>
						</p>
					</form>

				</div>
			</div>
		</div>
	</section>


</form:form>