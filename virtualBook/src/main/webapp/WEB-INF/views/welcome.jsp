<div id="container_demo">
	<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
	<div id="wrapper">
		<h1>Welcome</h1>
		<div class="container animate form" id="welcome" >
			<table class="menuSection">
				<tr>
					<td>
						<fieldset>
							<legend >
									VirtualBook
							</legend>						
							<table class="menuTable">
								<tr>
									<td>
										<a href="${pageContext.request.contextPath}/event" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/newEvent.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/client"><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/calendar.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/item" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/documents.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/user" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/charts.png"></a>
									</td>
								</tr>
								<tr>
									<td><h2>New Event</h2></td>
									<td><h2>Calendar</h2></td>
									<td><h2>Documents</h2></td>
									<td><h2>Graphics</h2></td>
								</tr>
							</table>
						</fieldset>	
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
							<legend>
								Next Events
							</legend>
							<table>
								<tr>
									<td>
										<section>
										
										</section>
									</td>
									<td>
										<section>
										
										</section>
									</td>
									<td>
										<section>
										
										</section>
									</td>
									<td>
										<section>
										
										</section>
									</td>
									
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
							<legend >
									Data
							</legend>						
							<table class="menuTable">
								<tr>
									<td>
										<a href="${pageContext.request.contextPath}/location" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/location.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/client"><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/customer.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/item" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/item.png"></a>
									</td>
									<td>
										<a href="${pageContext.request.contextPath}/user" ><img class="menuOption" alt="" src="${pageContext.request.contextPath}/resources/images/user.png"></a>
									</td>
								</tr>
								<tr>
									<td><h2>Locations</h2></td>
									<td><h2>Customers</h2></td>
									<td><h2>Items</h2></td>
									<td><h2>Users</h2></td>
								</tr>
							</table>
						</fieldset>	
					</td>
				</tr>
			</table>				
		</div>
	</div>
</div>
