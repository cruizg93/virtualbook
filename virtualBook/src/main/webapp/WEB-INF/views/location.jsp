<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="${pageContext.request.contextPath}/resources/css/dataTable_ui.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/dataTable.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
var context = '${pageContext.request.contextPath}';
</script>
<style>
	#wrapper input:not([type="checkbox"]):not([type="button"]){
		width:90%;
		padding: 10px 0px 10px 30px;
	}
	
	#wrapper p{
		padding-bottom: 0px;
	}
</style>
<script src="${pageContext.request.contextPath}/resources/js/location.js" type="text/javascript" > </script>
<form:form action="${pageContext.request.contextPath}/location/manageLocation" 
		 method="POST" modelAttribute="locationView" id="mainLocationForm">
		<div id="container_demo">
			<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
			<div id="wrapper">
				<!-- inicio -->
				<div class="container animate form" id="location" >
					<h1>${locationView.title}</h1>
					<div id="tabs" class="tabs">
						<nav>
							<ul>			
								<li><a href="#section-1" onclick="clearMessages();" class="icon-shop"><span>Managment</span></a></li>
								<li><a href="#section-2" onclick="clearMessages();" class="icon-cup"><span>List</span></a></li>
							</ul>
						</nav>
						<div class="content">
							<section id="section-1" class="animate form">
								<div class="mediabox">
										<form action="" style="height: 581px !important;">
											<table style="width:100%;" id="tblForm">
												<tr>
													<td>
														<p>
															<form:label for="location.name" class="uname" data-icon="u"
																path="location.name">Name Location</form:label>
															<form:input id="location.name" path="location.name" name="location.name"
																required="required" type="text" placeholder="Hilton"></form:input>
														</p>
													</td>
													<td>
														<p>
															<form:label path="location.phoneNumber" class="uname" data-icon="e">Number Phone</form:label>
															<form:input path="location.phoneNumber" name="location.phoneNumber" required="required"
																placeholder="813 000 4595" ></form:input>
															
														</p>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<p>
															<form:label path="location.address" class="uname" data-icon="p">Address</form:label>
															<form:input path="location.address" name="location.address" style="width:95% !important;"
																required="required" placeholder="31052 stone arch ave."></form:input>
								
														</p>
													</td>												
												</tr>
												<tr>
													<td colspan="2">
														<p class="action button">
														<form:button value="1" name="actionForm" id="btnSave" style="display: ${locationView.btnSave};">SAVE</form:button>
														<form:button value="3" name="actionForm" id="btnDelete" style="display: ${locationView.btnDelete};">DELETE</form:button>
														<input type="button"  value="CANCEL"  name="" id="btnCancel" onclick="cancelAction();" style="display: ${locationView.btnCancel};"/>
														</p>
													</td>
												</tr>
											</table>
											
										<form:hidden path="fromSearch" name="fromSearch" class="uname" value="" id="fromSearch"/>
										<form:hidden path="location.id" name="location.id" class="uname" value=""  />
										<form:hidden value="" id="actionForSearch" name="actionForm" path="actionForm"/>
										<form:hidden path="location.status" name="location.status" id="status" class="uname" value=""  />
										<form:hidden path="location.state" name="location.state" id="state" class="uname" value=""/>
										</form>
								</div>
							</section>
							<section id="section-2">
								<div class="mediabox">
									<table cellpadding="0" cellspacing="0" border="0" class="display" id="clientlist">
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="lcl" items="${locationView.locations}" varStatus="counter">
											<tr class="odd gradeA" style="cursor: pointer;" onclick="loadFromList('${lcl.id}')">    
										    	<td >${lcl.name}</td>
										    	<td >${lcl.phoneNumber}</td>
										    	<td style="width:38% !important;">${lcl.address}</td>
										    	<td >${lcl.status}</td>
										    </tr>
										</c:forEach>									
									</tbody>
									</table>
								</div>
							</section>
						</div><!-- /content -->
					</div><!-- /tabs -->
					<h2 id="message">${msg}</h2>
					<h2 id="messageError">${msgError}</h2>
				</div>	
			</div>
		</div>

</form:form>
<script src="${pageContext.request.contextPath}/resources/js/cbpFWTabs.js"></script>
<script>
	new CBPFWTabs( document.getElementById( 'tabs' ) );
</script>
	