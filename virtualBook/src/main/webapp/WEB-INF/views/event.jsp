<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="${pageContext.request.contextPath}/resources/js/event.js" type="text/javascript" > </script>
<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#datepicker").datepicker();
    $("#datepickerContract").datepicker();
  });
</script>

<form:form action="${pageContext.request.contextPath}/event/manageEvent" 
		 method="POST" modelAttribute="eventView" id="mainEventForm" style="height:900px;">
	<div id="container_demo">
		<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
		<div id="wrapper">
			<!-- inicio -->
			<div class="container animate form" id="events" >
				<h1></h1>
				<div id="tabs" class="tabs">
					<nav>
						<ul>			
							<li><a href="#section-1" onclick="clearMessages();" class="icon-shop"><span>Events</span></a></li>
							<li><a href="#section-2" onclick="clearMessages();" class="icon-cup"><span>List</span></a></li>
						</ul>
					</nav>
					<div class="content">
						<section id="section-1" class="animate form">
							<div class="mediabox">
								<form action="" style="height: 581px !important;">
									<table style="width:100%;" id="tblForm">
										<tr>
											<td>
												<p>
													<form:label for="event.date" class="uname" data-icon="u"
														path="event.date">Date Event</form:label>
													<form:input id="datepicker" path="event.date" name="event.date"
														required="required" type="text" placeholder="Date" onchange="validateDatePicker(this)"></form:input>
												</p>
											</td>
											<td>
												<p>
													<form:label for="client" class="uname" data-icon="u"
														path="event.client">Client</form:label>
													<form:select path="c" id="ddClient" items="${eventView.clients}" itemLabel="name" >
													   <!--<form:option value="NONE" label="--- Select ---"/>
													   <form:options items="${eventView.clients}"  itemLabel="namePhoneNumber" />-->
													</form:select>
												</p>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<p>
													<form:label for="locations" class="uname" data-icon="u"
														path="locations">Location</form:label>
													<form:select path="event.location" style="width: 96% !important;">
													   <form:option value="NONE" label="--- Select ---"/>
													   <form:options items="${eventView.locations}"  itemLabel="name"/>
													</form:select>
												</p>
											</td>
										</tr>	
										<tr>
											<td style="text-align:left;">
												<form:checkbox path="isClientContact" value="clientContract" style="margin-right:10px;" id="contacCheckBox"/>Contact same as client?
											</td>
										</tr>		
										<tr>
											<td>
												<p>
													<form:label for="event.contact.name" class="uname" data-icon="u"
														path="event.contact.name">Contact Person</form:label>
													<form:input id="contactName" path="event.contact.name" name="event.contact.name"
														required="required" type="text" placeholder="Contact Name"></form:input>
												</p>
											</td>
											<td>
												<p>
													<form:label for="event.contact.phoneNumber" class="uname" data-icon="u"
														path="event.contact.phoneNumber">Contact Phone </form:label>
													<form:input id="contactPhone" path="event.contact.phoneNumber" name="event.contact.phoneNumber"
														required="required" type="text" placeholder="Contact Phone"></form:input>
												</p>
											</td>
										</tr>
										<tr>
											<td style="text-align:left;">
												<form:checkbox path="hasContract" value="hasContract" id="hasContractCheckBox" style="margin-right:10px;"/>has contract?<form:input style="display:none;margin-right:1%;width:50%; float:right;" id="datepickerContract" path="" name=""
														type="text" placeholder="when the contract was sended?"></form:input>
											</td>
										</tr>
										<tr>
											<td>
												<p>
													<form:label for="items" class="uname" data-icon="u"
														path="items">Items</form:label>
													<form:select id="ItemCmb" path="event.item" >
													   <form:option value="NONE" label="--- Select ---"/>
													   <form:options items="${eventView.items}"   itemLabel="description" />
													</form:select>
												</p>
											</td>
											<td>
												<p style="width:42%; display:inline-block;">
													<form:label for="event.item.count" class="uname" data-icon="u"
														path="event.item.count">Quantity</form:label>
													<form:input style="width:75%;" id="count" path="event.item.count" name="event.item.count"
														type="text" placeholder="Count"></form:input>
												</p>
												<p style="width:36%;display:inline-block;">
													<form:label for="event.item.unitPrice" class="uname" data-icon="u"
														path="event.item.unitPrice">Unit Price</form:label>
													<form:input style="width:75%;" id="unitPrice" path="event.item.unitPrice" name="event.item.unitPrice"
														 type="text" placeholder="Unit Price"></form:input>
												</p>
												<p class="button" style="width:19%;display:inline-block;text-align: right;">
													<input type="button"  value="Add"  name="actionForm" id="btnAddItem" onclick="addItem()" style="padding:0px; width:90px !important;"/>
													
												</p>
											</td>
										</tr>
										<tr>
											<td><b id="ItemError" class="summaryError"></b></td>
											<td><b id="countError" style="float:left" class="summaryError"></b><b id="unitError" class="summaryError"></b></td>
										</tr>
										<tr>
											<td>
												<table  id="itemsTable">
													<thead>
														<tr>
															<th style="width:60% !important;">Item</th>
															<th style="width:20% !important;">Quantity</th>
															<th style="width:20% !important;">Unit Price</th>
														</tr>
													</thead>
													<tbody>
													</tbody>	
												</table>
												<p>
													<form:label for="event.notes" class="uname" data-icon="u" style="position:unset;"
														path="event.notes">Notes	</form:label>
													<form:textarea path="event.notes" rows="5" style="height:210px;border:1px solid rgb(178, 178, 178);width:100%;box-shadow:0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;padding: 15px; font-size:1em; font-family:comic sans ms;"/>
												</p>
											</td>
											<td>
												
												<table class="tblEventSumary">
													<tr>
														<td class="sumaryLabel">
															SubTotal
														</td>
														<td class="sumaryField">
															<form:input id="subTotal" path="event.subTotal" name="event.subTotal"
																required="required" type="text" placeholder="0" onchange="calculateTotal()"></form:input>	
															<form:hidden path="" name="hisubTotal" id="hisubTotal" class="uname" value="0"/>
														</td>
													</tr>
													<tr>
														<td class="sumaryLabel">
															Delivery
														</td>
														<td class="sumaryField">
															<form:input id="delivery" path="event.delivery" name="event.delivery"
																type="text" placeholder="0" onchange="calculateTotal()"></form:input>
															<form:hidden path="" name="hidDelivery" id="hiddelivery" class="uname" value="0"/>
														</td>
													</tr>
													<tr>
														<td class="sumaryLabel">
															Forward Payment
														</td>
														<td class="sumaryField">
															<form:input id="forwardPayment" path="event.forwardPayment" name="event.forwardPayment"
																type="text" placeholder="0" onchange="calculateTotal()"></form:input>
															<form:hidden path="" name="hidForwardPayment" id="hidforwardPayment" class="uname" value="0"/>
														</td>
													</tr>
													<tr>
														<td class="sumaryLabel">
															Tax %
														</td>
														<td class="sumaryField">
															<form:input id="taxes" path="event.taxes" name="event.taxes"
																 type="text" placeholder="0" onchange="calculateTotal()"></form:input>
															<form:hidden path="" name="hidTax" id="hidtaxes" class="uname" value="0"/>
														</td>
													</tr>
													<tr>
														<td class="sumaryLabel">
															Total
														</td>
														<td class="sumaryField">
															<form:input id="total" path="event.total" name="event.total"
																required="required" type="text" placeholder="0" onchange="calculateTotal()"></form:input>
															<form:hidden path="" name="hidtotal" id="hidtotal" class="uname" value="0"/>												
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<p class="action button">
												<form:button value="1" name="actionForm" id="btnSave" style="display: ${eventView.btnSave};">SAVE</form:button>
												<form:button value="3" name="actionForm" id="btnDelete" style="display: ${eventView.btnDelete};">DELETE</form:button>
												<input type="button"  value="CANCEL"  name="" id="btnCancel" onclick="cancelAction();" style="display: ${eventView.btnCancel};"/>
												</p>
											</td>
										</tr>
									</table>	
								</form>
							</div>
						</section>
						<section id="section-2">
							<div class="mediabox">
								<table cellpadding="0" cellspacing="0" border="0" class="display" id="clientlist">
									<tbody>								
									</tbody>
								</table>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</form:form>
<script src="${pageContext.request.contextPath}/resources/js/cbpFWTabs.js"></script>
<script>
	new CBPFWTabs( document.getElementById( 'tabs' ) );
</script>
