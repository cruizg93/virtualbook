<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link href="${pageContext.request.contextPath}/resources/css/main.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/mobile.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/menuApp.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/animate-custom.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />
	

<link href="${pageContext.request.contextPath}/resources/css/demo.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/component.css"
	rel="stylesheet" type="text/css" />
	
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/menu.js" type="text/javascript" > </script>
<script type="text/javascript" charset="utf-8">
var context = '${pageContext.request.contextPath}';
</script>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
	<table class="container">
		<tr>
			<td><tiles:insertAttribute name="header" /></td>
		</tr>
		<tr style="height: auto;">
			<td><tiles:insertAttribute name="body" /></td>
		</tr>	
		<tr>
			<td ><tiles:insertAttribute name="footer" /></td>
		</tr>
	</table>
</body>
</html>