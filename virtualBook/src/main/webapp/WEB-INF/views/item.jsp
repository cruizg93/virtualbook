<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="${pageContext.request.contextPath}/resources/css/dataTable_ui.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/dataTable.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/item.js" type="text/javascript" > </script>
<form:form action="${pageContext.request.contextPath}/item/manageItem" 
		 method="POST" modelAttribute="itemView" id="mainItemForm">
		
		<div id="container_demo">
			<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
			<div id="wrapper">
				<!-- inicio -->
				<div class="container animate form" id="location" >
					<h1>${itemView.title}</h1>
					<div id="tabs" class="tabs">
						<nav>
							<ul>			
								<li><a href="#section-1" onclick="clearMessages();" class="icon-shop"><span>Managment</span></a></li>
								<li><a href="#section-2" onclick="clearMessages();" class="icon-cup"><span>List</span></a></li>
							</ul>
						</nav>
						<div class="content">
							<section id="section-1" class="animate form">
								<div class="mediabox">
										<form action="" autocomplete="on">
										<table style="width:100%;" id="tblForm">
												<tr>
													<td>
														<p>
															<form:label for="item.description" class="uname" data-icon="u"
																path="item.description">Description</form:label>
															<form:input id="description" path="item.description" name="item.description"
																required="required" type="text" placeholder="Description"></form:input>
														</p>
													</td>
													<td>
														<p>
															<form:label for="item.count" class="uname" data-icon="u"
																path="item.count">Count</form:label>
															<form:input id="count" path="item.count" name="item.count"
																required="required" type="text" placeholder="Count"></form:input>			
														</p>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<p class="action button">
															<form:button value="1" name="actionForm" id="btnSave" style="display: ${itemView.btnSave};">SAVE</form:button>
															<form:button value="3" name="actionForm" id="btnDelete" style="display: ${itemView.btnDelete};">DELETE</form:button>
															<input type="button"  value="CANCEL"  name="" id="btnCancel" onclick="cancelAction();" style="display: ${itemView.btnCancel};"/>
														</p>													
													</td>
												</tr>
											</table>
											<form:hidden path="fromSearch" name="fromSearch" class="uname" value="" id="fromSearch"/>
											<form:hidden path="item.id" name="item.id" class="uname" value=""  />
											<form:hidden value="" id="actionForSearch" name="actionForm" path="actionForm"/>
											<form:hidden path="item.status" name="item.status" id="status" class="uname" value=""  />
											<form:hidden path="item.state" name="item.state" id="state" class="uname" value=""/>
										</form>
								</div>
							</section>
							<section id="section-2">
								<div class="mediabox">
									<table cellpadding="0" cellspacing="0" border="0" class="display" id="itemlist">
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="litem" items="${itemView.items}" varStatus="counter">
											<tr class="odd gradeA" style="cursor: pointer;" onclick="loadFromList('${litem.id}')">    
										    	<td >${litem.description}</td>
										    	<td >${litem.count}</td>
										    	<td >${litem.status}</td>
										    </tr>
										</c:forEach>									
									</tbody>
									</table>
								</div>
							</section>
						</div><!-- /content -->
					</div><!-- /tabs -->
					<h2 id="message">${msg}</h2>
					<h2 id="messageError">${msgError}</h2>
				</div>
				<script src="${pageContext.request.contextPath}/resources/js/cbpFWTabs.js"></script>
				<script>
					new CBPFWTabs( document.getElementById( 'tabs' ) );
				</script>
				<!-- fin -->
			</div>
		</div>
</form:form>