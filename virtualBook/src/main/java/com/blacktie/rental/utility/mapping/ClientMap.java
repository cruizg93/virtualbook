package com.blacktie.rental.utility.mapping;

import java.util.ArrayList;
import java.util.List;

import com.blacktie.rental.model.Client;
import com.blacktie.rental.pojo.ClientView;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class ClientMap {

	/**
	 * this method receive an Entity object and return a ClientView object with the same information.
	 * @param Client
	 * @return ClientView 
	 */
	public ClientView DAOToView(Client client){
		ClientView clientV = new ClientView();
		
		if(client != null){
			clientV.setCompanyName(client.getCompanyName());
			clientV.setEmail(client.getEmail());
			clientV.setId(client.getId());
			clientV.setPhoneNumber(client.getPhoneNumber());
			clientV.setLastName(client.getLastName());
			clientV.setName(client.getName());
			clientV.setState(client.getStatus());
			return clientV;
		}else{
			return null;
		}
	}
	/**
	 * this method receive an ClientView object and return a entity object with the 
	 * information to perform a DAO action.
	 * @param Client
	 * @return ClientView 
	 */
	public Client ViewToDAO(ClientView clientV){
		Client client = new Client();
		if(clientV !=null){
			client.setCompanyName(clientV.getCompanyName());
			client.setEmail(clientV.getEmail());
			client.setId(clientV.getId());
			client.setPhoneNumber(clientV.getPhoneNumber());
			client.setLastName(clientV.getLastName());
			client.setName(clientV.getName());
			client.setStatus(clientV.getState());
			
			return client;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive a List of entitys object and return a list of plain object with the same information
	 * @param List<Client>
	 * @return List<ClientView>
	 */
	public List<ClientView> LDaoToView(List<Client> client){
		ArrayList<ClientView> lReturn = new ArrayList<ClientView>();
		try {
			if(client!=null)
			for (Client cl: client) {
				lReturn.add(DAOToView(cl));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
	
	/**
	 * this method receive a List of plain object and return a list of entity objects with the information 
	 * to perform a dao action.
	 * @param List<ClientView>
	 * @return List<Client>
	 */
	public List<Client> LViewtoDao(List<ClientView>LClientV){
		ArrayList<Client> lReturn = new ArrayList<Client>();
		
		try {
			if(LClientV!=null)
			for (ClientView cv: LClientV) {
				lReturn.add(ViewToDAO(cv));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
}
