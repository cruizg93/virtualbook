package com.blacktie.rental.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class HibernateUtil {
	
	public static final SessionFactory sessionFactory = buildSessionFactory();
	
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	
	private static SessionFactory buildSessionFactory(){
		try {
			//create The sessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			//Make sure you log the Exception, as it mimght be swallowed
			System.err.println("Initial SessionFactory creation failes. "+ e);
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
}
