package com.blacktie.rental.utility.mapping;

import java.util.ArrayList;
import java.util.List;

import com.blacktie.rental.model.Item;
import com.blacktie.rental.pojo.ItemForm;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class ItemMap {

	/**
	 * this method receive an Entity object and return an EventForm object with the same information.
	 * @param Item
	 * @return ItemForm
	 */
	public ItemForm DAOToFrm(Item item){
		ItemForm itemF = new ItemForm();
		
		if(item != null){
			itemF.setId(item.getId());
			itemF.setDescription(item.getDescription());
			itemF.setState(item.getStatus());
			itemF.setCount(item.getCount());
			return itemF;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive an ItemForm object and return a entity object with the 
	 * information to perform a DAO action.
	 * @param ItemForm
	 * @return Item
	 */
	public Item FrmToDAO(ItemForm itemF){
		Item item= new Item();
		if(itemF !=null){
			item.setDescription(itemF.getDescription());
			item.setId(itemF.getId());
			item.setStatus(itemF.getState());
			item.setCount(itemF.getCount());
			return item;
		}else{
			return null;
		}
	}
	/**
	 * this method receive a List of entitys object and return a list of plain object with the same information
	 * @param List<Item>
	 * @return List<ItemForm>
	 */
	public List<ItemForm> LDaoToFrm(List<Item> item){
		ArrayList<ItemForm> lReturn = new ArrayList<ItemForm>();
		
		try {
			if(item!=null)
			for (Item x: item) {
				lReturn.add(DAOToFrm(x));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}

	
	/**
	 * this method receive a List of plain object and return a list of entity objects with the information 
	 * to perform a dao action.
	 * @param List<ItemForm>
	 * @return List<Item>
	 */
	public List<Item> LFrmtoDao(List<ItemForm> lItem){
		ArrayList<Item> lReturn = new ArrayList<Item>();
		try {
			if(lItem!=null)
			for (ItemForm ll: lItem) {
				lReturn.add(FrmToDAO(ll));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lReturn;
	}
}
