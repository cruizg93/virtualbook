package com.blacktie.rental.utility.mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import com.blacktie.rental.model.Event;
import com.blacktie.rental.model.Item;
import com.blacktie.rental.pojo.EventForm;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class EventMap {

	/**
	 * <i>cmap<i> is the object used to map Clients, between ClientView and Client(entity)
	 */
	ClientMap cmap;
	/**
	 * <i>lmap</i> is the object used to map Locations, between LocationForm and Location(entity)
	 */
	LocationMap lmap;
	/**
	 * <i>imap</i> is the object uset to map Items, between  ItemForm and Item(entity)
	 */
	ItemMap imap;
	
	/**
	 * Default constructor.
	 */
	public EventMap(){
		cmap = new ClientMap();
		lmap = new LocationMap();
		imap = new ItemMap();
	}
	
	/**
	 * this method receive an Entity object and return an EventForm object with the same information.
	 * @param Event
	 * @return EventForm
	 */
	public EventForm DAOToFrm(Event event){
		EventForm form = new EventForm();
		if(event != null){
			form.setClient(cmap.DAOToView(event.getClient()));
			form.setContact(event.getContactName(),event.getContactPhone());
			form.setDate(event.getDate());
			form.setDelivery(event.getDelivery());
			form.setForwardPayment(event.getForwardPayment());
			form.setItemsEvent(imap.LDaoToFrm(new ArrayList(event.getItems())));
			form.setLocation(lmap.DAOToFrm(event.getLocation()));
			form.setNotes(event.getNotes());
			form.setTaxes(event.getTaxes());
			return form;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive an EventForm object and return a entity object with the 
	 * information to perform a DAO action.
	 * @param EventForm
	 * @return Event
	 */
	public Event FrmToDAO(EventForm form){
		Event event= new Event();
		if(form !=null){
			event.setClient(cmap.ViewToDAO(form.getClient()));
			event.setContactName(form.getContact().getName());
			event.setContactPhone(form.getContact().getPhoneNumber());
			event.setContract(event.getContract());
			event.setDate(form.getDate());
			event.setDelivery(form.getDelivery());
			event.setForwardPayment(form.getForwardPayment());
			event.setItems(new HashSet<Item>(imap.LFrmtoDao(form.getItemsEvent())));
			event.setLocation(lmap.FrmToDAO(form.getLocation()));
			event.setNotes(form.getNotes());
			event.setTaxes(form.getTaxes());
			return event;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive a List of entitys object and return a list of plain object with the same information
	 * @param List<Event>
	 * @return List<EventForm>
	 */
	public List<EventForm> LDaoToFrm(List<Event> event){
		ArrayList<EventForm> lReturn = new ArrayList<EventForm>();
		
		try {
			if(event != null)
			for (Event x: event ) {
				lReturn.add(DAOToFrm(x));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
	/**
	 * this method receive a List of plain object and return a list of entity objects with the information 
	 * to perform a dao action.
	 * @param List<EventForm>
	 * @return List<Event>
	 */
	public List<Event> LFrmtoDao(List<EventForm> Levent){
		ArrayList<Event> lReturn = new ArrayList<Event>();
		
		try {
			if(Levent!=null)
			for (EventForm ll: Levent) {
				lReturn.add(FrmToDAO(ll));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
	
	
}
