package com.blacktie.rental.utility.mapping;

import java.util.ArrayList;
import java.util.List;

import com.blacktie.rental.model.Location;
import com.blacktie.rental.pojo.LocationForm;

/**
 * 
 * @author Cristian Ruiz
 * 
 */
public class LocationMap {

	
	/**
	 * this method receive an Entity object and return an LocationForm object with the same information.
	 * @param Location
	 * @return LocationForm
	 */
	public LocationForm DAOToFrm(Location location){
		LocationForm locationF = new LocationForm();
		
		if(location != null){
			locationF.setAddress(location.getAddress());
			locationF.setId(location.getId());
			locationF.setName(location.getNameLocation());
			locationF.setPhoneNumber(location.getPhoneNumber());
			locationF.setState(location.getStatus());
			return locationF;
		}else{
			return null;
		}
	}
	/**
	 * this method receive an LocationForm object and return a entity object with the 
	 * information to perform a DAO action.
	 * @param LocationForm
	 * @return Location
	 */
	public Location FrmToDAO(LocationForm locationF){
		Location location= new Location();
		if(locationF !=null){
			location.setAddress(locationF.getAddress());
			location.setId(locationF.getId());
			location.setNameLocation(locationF.getName());
			location.setPhoneNumber(locationF.getPhoneNumber());
			location.setStatus(locationF.getState());
			return location;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive a List of entitys object and return a list of plain object with the same information
	 * @param List<Location>
	 * @return List<LocationForm>
	 */
	public List<LocationForm> LDaoToFrm(List<Location> location){
		ArrayList<LocationForm> lReturn = new ArrayList<LocationForm>();
		
		try {
			if(location != null)
			for (Location x: location ) {
				lReturn.add(DAOToFrm(x));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
	/**
	 * this method receive a List of plain object and return a list of entity objects with the information 
	 * to perform a dao action.
	 * @param List<LocationForm>
	 * @return List<Location>
	 */
	public List<Location> LFrmtoDao(List<LocationForm> Llocation){
		ArrayList<Location> lReturn = new ArrayList<Location>();
		
		try {
			if(Llocation!=null)
			for (LocationForm ll: Llocation) {
				lReturn.add(FrmToDAO(ll));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lReturn;
	}
}
