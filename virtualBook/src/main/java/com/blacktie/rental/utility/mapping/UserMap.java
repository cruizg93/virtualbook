package com.blacktie.rental.utility.mapping;

import com.blacktie.rental.model.User;
import com.blacktie.rental.pojo.UserView;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class UserMap {

	/**
	 * this method receive an Entity object and return an USerView object with the same information.
	 * @param User
	 * @return UserView
	 */
	public UserView DAOToView(User userM){
		UserView user = new UserView();
		
		if(userM!=null){
			user.setId(userM.getId());
			user.setPassword(userM.getPassword());
			user.setUser(userM.getUser());
			return user;
		}else{
			return null;
		}
	}
	
	/**
	 * this method receive an UserView object and return a entity object with the 
	 * information to perform a DAO action.
	 * @param UserView
	 * @return User
	 */
	public User ViewToDAO(UserView userV){
		User user = new User();
		
		if(userV != null){
			user.setId(userV.getId());
			user.setPassword(userV.getPassword());
			user.setUser(userV.getUser());
		}else{
			return null;
		}
		
		return user;
	}
}
