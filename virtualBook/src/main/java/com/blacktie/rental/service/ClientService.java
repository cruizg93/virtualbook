package com.blacktie.rental.service;

import java.util.List;

import com.blacktie.rental.pojo.ClientView;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public interface ClientService {
	/**
	 * <B>CLIENT SAVE</B>
	 * @param clienV
	 * @return client
	 */
	public ClientView save(ClientView clienV);

	/**
	 * <B>CLIENT LIST</B>
	 * @return client list
	 */
	public List<ClientView> list();

	/**
	 * <B>CLIENT SEARCH</B>
	 * @param client
	 * @return client
	 */
	ClientView search(ClientView clientV);

	/**
	 * <B>CLIENT UPDATE</B>
	 * @param client
	 * @return client
	 */
	ClientView update(ClientView clientV);
	
	/**
	 * <B>CLIENT DELETE</B>
	 * @param client 
	 * @return client
	 */
	ClientView delete(ClientView clientV);
}
