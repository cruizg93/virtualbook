package com.blacktie.rental.service;

import java.util.List;

import com.blacktie.rental.dao.LocationDAO;
import com.blacktie.rental.dao.LocationDAOImpl;
import com.blacktie.rental.pojo.LocationForm;
import com.blacktie.rental.utility.mapping.LocationMap;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class LocationServiceImpl implements LocationService{

	/**
	 * <i>dao</i> is the Data Access object 
	 */
	private LocationDAO dao;
	/**
	 * <i>map</i> is use to map the objects between plain objects to entitys.
	 */
	private LocationMap map;
	/**
	 * Default Constructor
	 */
	public LocationServiceImpl() {
		dao = new LocationDAOImpl();
		map = new LocationMap();
	}
	
	/**
	 * <B> SAVE LOCATION </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.save and receive the result to return it back to controller
	 * @param LocationForm(not null)
	 * @return LocationForm, same object if is successful, if not return null.
	 */
	@Override
	public LocationForm save(LocationForm loc) {
		try {
			loc = map.DAOToFrm(dao.save(map.FrmToDAO(loc)));
		} catch (Exception e) {
			loc = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return loc;
	}

	/**
	 * <B> UPDATE LOCATION</B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.update and receive the result to return it back to controller
	 * @param LocationForm (not null)
	 * @return LocationForm, same object if is successful, if not return null.
	 */
	@Override
	public LocationForm update(LocationForm loc) {
		try {
			loc = map.DAOToFrm(dao.update(map.FrmToDAO(loc)));
		} catch (Exception e) {
			loc = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return loc;
	}
	

	/**
	 * <B> DELETE LOCATION </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.delete and receive the result to return it back to controller
	 * @param LocationForm(not null)
	 * @return LocationForm, same object if is successful, if not return null.
	 */
	@Override
	public LocationForm delete(LocationForm loc) {
		try {
			loc.setState(0);
			loc = map.DAOToFrm(dao.delete(map.FrmToDAO(loc)));
		} catch (Exception e) {
			loc = null;
			e.printStackTrace();
		}
		return loc;
	}

	/**
	 * <B> LIST LOCATION</B>
	 * This method call DAO.list and capture an entity list than contain all the clients with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return List<LocationForm>.
	 */
	@Override
	public List<LocationForm> list() {
		List<LocationForm> lLoc = null;
		try { 
			lLoc = map.LDaoToFrm(dao.list());
		} catch (Exception e) {
			lLoc = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return lLoc;
	}

	/**
	 * <B> SEARCH LOCATION </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.search and receive the result to return it back to controller
	 * @param LocationForm (not null)
	 * @return LocationForm, same object if is successful, if not return null.
	 */
	@Override
	public LocationForm search(LocationForm loc) {
		List<LocationForm> locationList = null;
		try {
			locationList = map.LDaoToFrm(dao.search(map.FrmToDAO(loc)));
			if(locationList.size()>0){
				loc = locationList.get(0);
			}else{
				loc = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return loc;
	}
}