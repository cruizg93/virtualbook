package com.blacktie.rental.service;

import java.util.List;
import com.blacktie.rental.dao.ClientDAO;
import com.blacktie.rental.dao.ClientDAOImpl;
import com.blacktie.rental.pojo.ClientView;
import com.blacktie.rental.utility.mapping.ClientMap;

/**
 * 
 * @author Cristian Ruiz
 * 
 */
public class ClientServiceImpl implements ClientService {

	/**
	 * <i>dao</i> is the Data Access object 
	 */
	private ClientDAO dao;
	/**
	 * <i>map</i> is use to map the objects between plain objects to entitys.
	 */
	private ClientMap map;
	
	/**
	 * Default constructor.
	 */
	public ClientServiceImpl() {
		dao = new ClientDAOImpl();
		map = new ClientMap();
	}

	/**
	 * <B> SAVE CLIENT </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.save and receive the result to return it back to controller
	 * @param ClientView (not null)
	 * @return ClientView, same object if is successful, if not return null.
	 */
	@Override
	public ClientView save(ClientView clientV) {
	
		try {
			clientV = map.DAOToView(dao.save(map.ViewToDAO(clientV)));
		} catch (Exception e) {
			clientV = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return clientV;
	}
	
	/**
	 * <B> UPDATE CLIENT </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.update and receive the result to return it back to controller
	 * @param ClientView (not null)
	 * @return ClientView, same object if is successful, if not return null.
	 */
	@Override
	public ClientView update(ClientView clientV){
		try {
			clientV = map.DAOToView(dao.update(map.ViewToDAO(clientV)));
		} catch (Exception e) {
			clientV = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return clientV;
	}
	
	/**
	 * <B> LIST CIENTS</B>
	 * This method call DAO.list and capture an entity list than contain all the clients with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return List<ClientView>.
	 */
	@Override
	public List<ClientView> list() {
		List<ClientView> clientList = null;
		try {
			clientList = map.LDaoToView(dao.list());
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return clientList;
	}

	/**
	 * <B> SEARCH CLIENT </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.search and receive the result to return it back to controller
	 * @param ClientView (not null)
	 * @return ClientView, same object if is successful, if not return null.
	 */
	@Override
	public ClientView search(ClientView clientV){
		List<ClientView> clientList = null;
		try {
			clientList = map.LDaoToView(dao.search(map.ViewToDAO(clientV)));
			if(clientList.size()>0){
				clientV = clientList.get(0);
			}else{
				clientV = null;
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return clientV;
	}
	
	/**
	 * <B> DELETE CLIENT </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.delete and receive the result to return it back to controller
	 * @param ClientView (not null)
	 * @return ClientView, same object if is successful, if not return null.
	 */
	@Override
	public ClientView delete(ClientView clientV){
		try {
			clientV.setState(0);
			clientV = map.DAOToView(dao.delete(map.ViewToDAO(clientV)));
		} catch (Exception e) {
			clientV = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return clientV;
	}
}






