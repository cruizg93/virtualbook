package com.blacktie.rental.service;

import java.util.List;

import com.blacktie.rental.pojo.LocationForm;

public interface LocationService {

	/**
	 * <B>UPDATE LOCATION</B>
	 * @param LocationForm
	 * @return LocationForm
	 */
	LocationForm update(LocationForm loc);
	/**
	 * <B>SAVE LOCATION</B>
	 * @param LocationForm
	 * @return LocationForm
	 */
	LocationForm save(LocationForm loc);
	/**
	 * <B>LIST LOCATIONS</B>
	 * @return List<LocationForm>
	 */
	List<LocationForm> list();
	/**
	 * <B>SEARCH LOCATION</B>
	 * @param LocationForm
	 * @return LocationForm
	 */
	LocationForm search(LocationForm loc);
	/**
	 * <B>DELETE LOCATION</B>
	 * @param LocationForm
	 * @return LocationForm
	 */
	LocationForm delete(LocationForm loc);

}
