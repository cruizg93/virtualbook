package com.blacktie.rental.service;

import com.blacktie.rental.dao.UserDAO;
import com.blacktie.rental.dao.UserDAOImpl;
import com.blacktie.rental.pojo.UserView;
import com.blacktie.rental.utility.mapping.UserMap;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class UserServiceImpl implements UserService{

	/**
	 * <i>dao</i> is the Data Access object 
	 */
	private UserDAO dao;
	/**
	 * <i>map</i> is use to map the objects between plain objects to entitys.
	 */
	private UserMap map;
	
	/**
	 * Default constructor.
	 */
	public  UserServiceImpl() {
		dao = new UserDAOImpl();
		map = new UserMap();
	}
	
	/**
	 * <B> LOGIN USER</B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.findByObject and receive the result to return it back to controller
	 * @param UserView (not null)
	 * @return UserView, same object if is successful, if not return null.
	 */
	@Override
	public UserView login(UserView user) {
		try {
			user = map.DAOToView(dao.findByObject(map.ViewToDAO(user)));
		} catch (Exception e) {
			user=null;
			System.out.println(e);
			e.printStackTrace();
		}
		return user;
	}

}
