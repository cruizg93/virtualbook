package com.blacktie.rental.service;

import java.util.List;

import com.blacktie.rental.pojo.ItemForm;

/**
 * 
 * @author Cristian Ruiz 
 *
 */

public interface ItemService {

	/**
	 * <B>ITEM UPDATE</B>
	 * @param itemForm
	 * @return itemForm
	 */
	ItemForm update(ItemForm it);

	/**
	 * <B>ITEM SAVE</B>
	 * @param itemForm
	 * @return itemForm
	 */
	ItemForm save(ItemForm it);

	/**
	 * <B>ITEM LIST</B>
	 * @return itemform list
	 */
	List<ItemForm> list();

	/**
	 * <B>ITEM SEARCH </B>
	 * @param itemForm
	 * @return itemform
	 */
	ItemForm search(ItemForm item);
	
	/**
	 * <B> ITEM DELETE</B>
	 * @param itemform
	 * @return itemForm
	 */
	ItemForm delete(ItemForm item);
}
