package com.blacktie.rental.service;

import java.util.List;
import com.blacktie.rental.dao.ClientDAO;
import com.blacktie.rental.dao.ClientDAOImpl;
import com.blacktie.rental.dao.EventDAO;
import com.blacktie.rental.dao.EventDAOImpl;
import com.blacktie.rental.dao.ItemDAO;
import com.blacktie.rental.dao.ItemDAOImpl;
import com.blacktie.rental.dao.LocationDAO;
import com.blacktie.rental.dao.LocationDAOImpl;
import com.blacktie.rental.model.Client;
import com.blacktie.rental.model.Item;
import com.blacktie.rental.model.Location;
import com.blacktie.rental.pojo.ClientView;
import com.blacktie.rental.pojo.EventForm;
import com.blacktie.rental.pojo.EventView;
import com.blacktie.rental.pojo.ItemForm;
import com.blacktie.rental.pojo.LocationForm;
import com.blacktie.rental.utility.mapping.ClientMap;
import com.blacktie.rental.utility.mapping.EventMap;
import com.blacktie.rental.utility.mapping.ItemMap;
import com.blacktie.rental.utility.mapping.LocationMap;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class EventServiceImpl implements EventService {
	
	/**
	 * <i>dao</i> is the Data Access object 
	 */
	EventDAO dao;
	/**
	 * <i>map</i> is use to map the objects between plain objects to entitys.
	 */
	EventMap map;
	
	/**
	 * default constructor
	 */
	public EventServiceImpl(){
		dao = new EventDAOImpl();
		map = new EventMap();
	}
	
	/**
	 * <B> EVENT LOAD ITEMS</B>
	 *  This method call DAO.list and capture an entity list than contain all the items with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return ItemForm list.
	 */
	private List<ItemForm> loadItems(){
		ItemDAO dao = new ItemDAOImpl();
		List<Item> list = null;
		ItemMap map = new ItemMap();
		try {
			list = dao.listEnable();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(list!=null){
			return map.LDaoToFrm(list);
		}else{
			return null;
		}
		
	}

	/**
	 * <B> EVENT LOAD LOCATIONS</B>
	 *  This method call DAO.list and capture an entity list than contain all the locations with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return locationForm list.
	 */
	private List<LocationForm> loadLocations(){
		LocationDAO dao = new LocationDAOImpl();
		List<Location> list = null;
		LocationMap map = new LocationMap();
		try {
			list = dao.listEnable();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(list!=null){
			return map.LDaoToFrm(list);
		}else{
			return null;
		}
	}
	
	/**
	 * <B> EVENT LOAD CLIENTS</B>
	 *  This method call DAO.list and capture an entity list than contain all the clients with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return clientView list.
	 */
	private List<ClientView> loadClient(){
		ClientDAO dao = new ClientDAOImpl();
		List<Client> list = null;
		ClientMap map = new ClientMap();
		try {
			list = dao.listEnable();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(list!=null){
			return map.LDaoToView(list);
		}else{
			return null;
		}
	}
	
	/**
	 * <B> EVENT LOAD CONFIG</B>
	 *  This method call all the load method than event needs to initial show up
	 * @return eventView.
	 */
	public EventView loadConfig(EventView view){
		try {
			view.setItems(loadItems());
			view.setLocations(loadLocations());
			view.setClients(loadClient());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	/**
	 * <B> SAVE EVENT</B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.save and receive the result to return it back to controller
	 * @param EventView(not null)
	 * @return EventView, same object if is successful, if not return null.
	 */
	@Override
	public EventView save(EventView view) {
		EventForm form = null;
		try {
			view.getEvent().print();
			//form = map.DAOToFrm(dao.save(map.FrmToDAO(view.getEvent())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

