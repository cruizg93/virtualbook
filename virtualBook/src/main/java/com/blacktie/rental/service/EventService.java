package com.blacktie.rental.service;

import com.blacktie.rental.pojo.EventView;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public interface EventService {

	/**
	 * <B> EVENT loadConfig</B>
	 * @param EventView
	 * @return EventView
	 */
	public EventView loadConfig(EventView view);
	
	/**
	 * <B> EVENT SAVE </B>
	 * @param EventView
	 * @return EventView
	 */
	public EventView save(EventView view);
}
