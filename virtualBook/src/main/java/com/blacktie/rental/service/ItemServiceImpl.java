package com.blacktie.rental.service;

import java.util.List;

import com.blacktie.rental.dao.ItemDAO;
import com.blacktie.rental.dao.ItemDAOImpl;
import com.blacktie.rental.pojo.ItemForm;
import com.blacktie.rental.utility.mapping.ItemMap;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class ItemServiceImpl implements ItemService {

	/**
	 * <i>dao</i> is the Data Access object 
	 */
	private ItemDAO dao;
	/**
	 * <i>map</i> is use to map the objects between plain objects to entitys.
	 */
	private ItemMap map;
	
	/**
	 * default constructor
	 */
	public ItemServiceImpl() {
		dao = new ItemDAOImpl();
		map = new ItemMap();
	}

	/**
	 * <B> SAVE ITEM</B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.save and receive the result to return it back to controller
	 * @param ItemForm (not null)
	 * @return ItemForm, same object if is successful, if not return null.
	 */
	@Override
	public ItemForm save(ItemForm it) {
		try {
			it = map.DAOToFrm(dao.save(map.FrmToDAO(it)));
		} catch (Exception e) {
			it = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return it;
	}

	/**
	 * <B> UPDATE ITEM </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.update and receive the result to return it back to controller
	 * @param ItemForm (not null)
	 * @return ItemForm, same object if is successful, if not return null.
	 */
	@Override
	public ItemForm update(ItemForm it) {
		try {
			it = map.DAOToFrm(dao.update(map.FrmToDAO(it)));
		} catch (Exception e) {
			it = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return it;
	}

	/**
	 * <B> DELETE ITEM</B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.delete and receive the result to return it back to controller
	 * @param ItemForm (not null)
	 * @return ItemForm, same object if is successful, if not return null.
	 */
	@Override
	public ItemForm delete(ItemForm item) {
		try {
			item.setState(0);
			item = map.DAOToFrm(dao.delete(map.FrmToDAO(item)));
		} catch (Exception e) {
			item = null;
			e.printStackTrace();
		}
		return item;
	}
	
	/**
	 * <B> LIST ITEMS</B>
	 * This method call DAO.list and capture an entity list than contain all the clients with status enable, 
	 * transform into a POJO list and return it back to controller
	 * @return List<ItemForm>.
	 */
	@Override
	public List<ItemForm> list() {
		List<ItemForm> lItem = null;
		try {
			lItem = map.LDaoToFrm(dao.list());
		} catch (Exception e) {
			lItem = null;
			System.out.println(e);
			e.printStackTrace();
		}
		return lItem;
	}

	/**
	 * <B> SEARCH ITEM </B>
	 * This method receive the POJO, then transform into an entity, 
	 * send that object to DAO.search and receive the result to return it back to controller
	 * @param ItemForm (not null)
	 * @return ItemForm, same object if is successful, if not return null.
	 */
	@Override
	public ItemForm search(ItemForm item) {
		List<ItemForm> itemList = null;
		try {
			itemList = map.LDaoToFrm(dao.search(map.FrmToDAO(item)));
			if(itemList.size()>0){
				item = itemList.get(0);
			}else{
				item = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return item;
	}
}
