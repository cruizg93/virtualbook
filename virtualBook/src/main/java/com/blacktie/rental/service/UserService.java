package com.blacktie.rental.service;

import com.blacktie.rental.pojo.UserView;

public interface UserService {
	
	/**
	 * <B>LOGIN USER</B>
	 * @param UserView
	 * @return UserView
	 */
	public UserView login(UserView user);
}
