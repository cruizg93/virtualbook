package com.blacktie.rental.dao;

import java.util.List;

import com.blacktie.rental.model.Item;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public interface ItemDAO {

	/**
	 * <B>ITEM SAVE</B>
	 * @param item
	 * @return item
	 */
	public Item save(Item item);
	
	/**
	 * <B>ITEM LIST</B>
	 * @return item list
	 */
	List<Item> list();
	/**
	 * <B> ITEM LISTENABLE</B>
	 * @return item list
	 */
	List<Item> listEnable();
	/**
	 * <B>ITEM SEARCH</B>
	 * @param item
	 * @return item
	 */
	List<Item> search(Item item);
	/**
	 * <B>ITEM UPDATE</B>
	 * @param item
	 * @return item
	 */
	Item update(Item item);

	/***
	 * <B>ITEM DELETE</B>
	 * @param item
	 * @return
	 */
	Item delete(Item item);
	
}
