package com.blacktie.rental.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.blacktie.rental.model.User;
import com.blacktie.rental.utility.HibernateUtil;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class UserDAOImpl implements UserDAO{

	/**
	 * <B> USER FIND BY OBJECT</B>
	 * @param User
	 * @return USer
	 * this method return an Object user if found but if not return null
	 */
	@Override
	public User findByObject(User user) {
		Transaction trns = null;
		Session session = null;
		boolean exist = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Query query = session.createQuery("from User where user = :user and password = :password");
			query.setString("user", user.getUser());
			query.setString("password", user.getPassword());
			Object object = query.uniqueResult();
			if(object != null){
				user = (User)object;
			}else{
				user=null;
			}
			trns.commit();
		} catch (Exception e) {
			exist = false;
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return user;
	}

}
