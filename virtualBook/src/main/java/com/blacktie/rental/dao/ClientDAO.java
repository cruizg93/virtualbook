package com.blacktie.rental.dao;

import java.util.ArrayList;
import java.util.List;

import com.blacktie.rental.model.Client;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public interface ClientDAO {
		
	/**
	 * <B>CLIENT SAVE</B>
	 * @param client
	 * @return client
	 */
	public Client save(Client client);

	/**
	 * <B>CLIENT List</B>
	 * @return client list
	 */
	ArrayList<Client> list();

	/**
	 * <B>CLIENT SEARCH</B>
	 * @param client
	 * @return client list
	 */
	List<Client> search(Client client);

	/**
	 * <B>CLIENT UPDATE</B>
	 * @param client
	 * @return client
	 */
	Client update(Client client);

	/**
	 * <B>CLIENT DELETE</B> 
	 * @param client
	 * @return client
	 */
	Client delete(Client client);

	/**
	 * <B>CLIENT LISTENABLE</B>
	 * @return client list
	 */
	ArrayList<Client> listEnable();
}
