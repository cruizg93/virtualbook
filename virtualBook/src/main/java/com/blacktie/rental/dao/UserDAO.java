package com.blacktie.rental.dao;

import com.blacktie.rental.model.User;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public interface UserDAO {
	
	/**
	 * <B>USER FIND BY OBJECT</B>
	 * @param user
	 * @return user
	 */
	public User findByObject(User user);
}
