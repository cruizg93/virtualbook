package com.blacktie.rental.dao;

import java.util.List;
import com.blacktie.rental.model.Location;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public interface LocationDAO {
	
	/**
	 * <B> LOCATION SAVE</B>
	 * @param location
	 * @return location
	 */
	public Location save(Location location);

	/**
	 * <B> LOCATION LIST</B>
	 * @return location list
	 */
	List<Location> list();

	/**
	 * <B> LOCATION SEARCH</B>
	 * @param location
	 * @return location list
	 */
	List<Location> search(Location location);

	/**
	 * <B>LOCATION UPDATE</B>
	 * @param location
	 * @return location
	 */
	Location update(Location location);

	/**
	 * <B>LOCATION DELETE</B>
	 * @param location
	 * @return location
	 */
	Location delete(Location location);

	/**
	 * <B>LOCATION LISTENABLE</B>
	 * @return location list
	 */
	List<Location> listEnable();
}
