package com.blacktie.rental.dao;

import com.blacktie.rental.model.Event;


/**
 * 
 * @author Cristian Ruiz
 *
 */
public interface EventDAO {
	
	/**
	 * <B> EVENT SAVE </B>
	 * @param event
	 * @return event
	 */
	public Event save(Event event);

}
