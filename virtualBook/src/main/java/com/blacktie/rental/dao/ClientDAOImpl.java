package com.blacktie.rental.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.blacktie.rental.model.Client;
import com.blacktie.rental.utility.HibernateUtil;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class ClientDAOImpl implements ClientDAO{

	/**
	 * <B> CLIENT SAVE </B>
	 * @param client
	 * @return client
	 */
	@Override
	public Client save(Client client) {
		Transaction trns = null;
		Session session=null;
		List<Client> searchResult = null;
		try {
			searchResult = search(client);
			if(searchResult != null && searchResult.size()<1){
				client.setStatus(1);
				session = HibernateUtil.getSessionFactory().openSession();
				trns = session.beginTransaction();
				session.persist(client);
				trns.commit();
			}else{
				client = null;
			}
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			if(searchResult != null && searchResult.size()<1){
				session.flush();
				session.close();
			}
		}
		return client;
	}
	
	@Override
	public Client update(Client client){
		Transaction trns = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(client);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return client;
	}
	
	@Override
	public List<Client> search(Client client){
		Transaction trns = null;
		Session session = null;
		List<Client> result = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Client.class);
			if(client.getId()>0){
				criteria.add(Restrictions.like("id", client.getId()));
			}else{
				criteria.add(Restrictions.or(
				Restrictions.like("email", client.getEmail()),
				Restrictions.like("phoneNumber", client.getPhoneNumber())
				));
			}
			result = criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return result;
	}
	
	@Override
	public ArrayList<Client> list() {
		Transaction trns = null;
		Session session=null;
		ArrayList<Client> clients=null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Client.class);
			criteria.addOrder(Order.asc("name"));
			clients= criteria.list().isEmpty()?null:(ArrayList<Client>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return clients;
	}
	
	@Override
	public ArrayList<Client> listEnable() {
		Transaction trns = null;
		Session session=null;
		ArrayList<Client> clients=null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Client.class);
			criteria.add(Restrictions.or(
				Restrictions.like("status",1)
				));
			criteria.addOrder(Order.asc("name"));
			clients= criteria.list().isEmpty()?null:(ArrayList<Client>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return clients;
	}
	
	@Override
	public Client delete(Client client){
		Transaction trns = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(client);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return client;
	}

}
