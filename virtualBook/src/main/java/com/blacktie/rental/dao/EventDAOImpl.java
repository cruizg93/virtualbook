package com.blacktie.rental.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.blacktie.rental.model.Event;
import com.blacktie.rental.utility.HibernateUtil;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class EventDAOImpl implements EventDAO{

	/**
	 * SAVE EVENT
	 * @param Event
	 * @return Event if something get wrong return null
	 */
	@Override
	public Event save(Event event) {
		Transaction trns = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.persist(event);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			e.printStackTrace();
			event=null;
		}finally{
			session.flush();
			session.close();
		}
		return event;
	}

	
}
