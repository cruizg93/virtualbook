package com.blacktie.rental.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.blacktie.rental.model.Location;
import com.blacktie.rental.utility.HibernateUtil;

public class LocationDAOImpl implements LocationDAO{

	/**
	 * <B>LOCATION SAVE</B>
	 * this method search the element before save, if result comeback return null
	 * @param Location
	 * @return location if something get wrong return null
	 */
	@Override
	public Location save(Location location) {
		Transaction trns = null;
		Session session = null;
		List<Location> searchResult = null;
		try {
			searchResult = search(location);
			if(searchResult != null && searchResult.size()<1){
				location.setStatus(1);
				session = HibernateUtil.getSessionFactory().openSession();
				trns = session.beginTransaction();
				session.persist(location);
				trns.commit();
			}else{
				location = null;
			}
		} catch (Exception e) {
			trns.rollback();
			e.printStackTrace();
		}finally{
			if(searchResult != null && searchResult.size()<1){
				session.flush();
				session.close();
			}
		}
		return location;
	}

	/**
	 * <B>LOCATION LIST</B>
	 * this method return all the locations
	 * @return locations list
	 */
	@Override
	public List<Location> list() {
		Transaction trns = null;
		Session session = null;
		List<Location> loc = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Location.class);
			criteria.addOrder(Order.asc("nameLocation"));
			loc= criteria.list().isEmpty()?null:(ArrayList<Location>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return loc;
	}
	
	/**
	 * <B>LOCATION LIST ENABLE</B>
	 * this method return all the items with the state 1 (enable)
	 * @return itemList
	 */
	@Override
	public List<Location> listEnable() {
		Transaction trns = null;
		Session session = null;
		List<Location> loc = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Location.class);
			criteria.add(Restrictions.or(
					Restrictions.like("status",1)
					));
			criteria.addOrder(Order.asc("nameLocation"));
			loc= criteria.list().isEmpty()?null:(ArrayList<Location>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return loc;
	}

	/**
	 * <B>LOCATION SEARCH</B>
	 * @param location
	 * @return location list
	 * this method return an location searched by id
	 */
	@Override
	public List<Location> search(Location location) {
 		Transaction trns = null;
		Session session = null;
		List<Location> result = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Location.class);
			if(location.getId()>0){
				criteria.add(Restrictions.like("id", location.getId()));
			}else{
				criteria.add(Restrictions.or(
						Restrictions.like("nameLocation", location.getNameLocation()),
						Restrictions.like("address", location.getAddress())
						));
			}
			
			result = criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return result;
	}
	
	/**
	 * <B>LOCATION UPDATE</B>
	 * @param location
	 * @return location
	 * This method update an existing record
	 */
	@Override
	public Location update(Location location) {
		Transaction trns = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(location);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return location;
	}

	/**
	 * <B>LOCATION DELETE</B>
	 * @param location
	 * @return location
	 * this method does an update in the record at the field state with value 0
	 */
	@Override
	public Location delete(Location location) {
		Transaction trns = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(location);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println("***"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return location;
	}
}