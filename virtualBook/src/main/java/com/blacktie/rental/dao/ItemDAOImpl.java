package com.blacktie.rental.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import com.blacktie.rental.model.Item;
import com.blacktie.rental.utility.HibernateUtil;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class ItemDAOImpl implements ItemDAO{

	/**
	 * ItemDao Transaction
	 */
	Transaction trns;
	/**
	 * ItemDao Session
	 */
	Session session;
	
	/**
	 * Default constructor
	 */
	public ItemDAOImpl() {
		trns = null;
		session = null;
	}
	
	/**
	 * <B>ITEM SAVE</B>
	 * this method search the element before save, if result comeback return null
	 * @param item
	 * @return item if something get wrong return null
	 */
	@Override
	public Item save(Item item) {
		trns = null;
		session = null;
		List<Item> searchResult = null;
		
		try {
			searchResult = search(item);
			if(searchResult != null && searchResult.size()<1){
				item.setStatus(1);
				session = HibernateUtil.getSessionFactory().openSession();
				trns = session.beginTransaction();
				session.persist(item);
				trns.commit();
			}else{
				item = null;
			}
		} catch (Exception e) {
			trns.rollback();
			e.printStackTrace();
		}finally{
			if(searchResult != null && searchResult.size()<1){
				session.flush();
				session.close();
			}
		}
		return item;
	}

	/**
	 * <B>ITEM LIST</B>
	 * this method return all the items 
	 * @return item list
	 */
	@Override
	public List<Item> list() {
		trns = null;
		session = null;
		List<Item> item = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Item.class);
			criteria.addOrder(Order.asc("description"));
			item = criteria.list().isEmpty()?null:(ArrayList<Item>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println(this.getClass().toString()+":"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return (ArrayList<Item>) item;
	}
	
	/**
	 * <B>ITEM LIST ENABLE</B>
	 * this method return all the items with the state 1 (enable)
	 * @return itemList
	 */
	@Override
	public List<Item> listEnable() {
		trns = null;
		session = null;
		List<Item> item = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Item.class);
			criteria.add(Restrictions.or(
				Restrictions.like("status",1)
				));
			criteria.addOrder(Order.asc("description"));
			item = criteria.list().isEmpty()?null:(ArrayList<Item>) criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println(this.getClass().toString()+":"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return (ArrayList<Item>) item;
	}
	
	/**
	 * <B>ITEM SEARCH</B>
	 * @param item
	 * @return item list
	 * this method return an item searched by id
	 */
	@Override
	public List<Item> search(Item item) {
		trns = null;
		session = null;
		List<Item> result = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			Criteria criteria = session.createCriteria(Item.class);
			if(item.getId()>0){
				criteria.add(Restrictions.like("id", item.getId()));
			}else{
				criteria.add(Restrictions.or(Restrictions.like("description", item.getDescription())));
			}
			
			result = criteria.list();
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return result;
	}

	/**
	 * <B>ITEM UPDATE</B>
	 * @param item
	 * @return item
	 * This method update an existing record
	 */
	@Override
	public Item update(Item item) {
		trns = null;
		session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(item);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println(this.getClass().toString()+":"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return item;
	}

	/**
	 * <B>ITEM DELETE</B>
	 * @param item
	 * @return item
	 * this method does an update in the record at the field state with value 0
	 */
	@Override
	public Item delete(Item item) {
		trns = null;
		session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trns = session.beginTransaction();
			session.update(item);
			trns.commit();
		} catch (Exception e) {
			trns.rollback();
			System.out.println(this.getClass().toString()+":"+e.getMessage());
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		return item;
	}
}