package com.blacktie.rental.pojo;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class UserView {

	/**
	 * User id 
	 */
	private int id;
	/**
	 * User nickname
	 */
	private String user;
	/**
	 * User password
	 */
	private String password;
	
	/**
	 * Get the user id
	 * @return Id 
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Set the user id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Get the user nickname
	 * @return user
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * Set the user nickname
	 * @param user
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * Get the user password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Set The user password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
