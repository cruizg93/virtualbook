package com.blacktie.rental.pojo;
import java.util.List;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class LocationView {

	/**
	 * is the ID of the location you want to show on the form
	 */
	private int fromSearch;
	
	/**
	 * is the object for the main form 
	 */
	private LocationForm location;
	/**
	 * contain a list of all the locations enable to work with 
	 */
	private List<LocationForm> locations;
	
	/**
	 * is use at the property css display for the button delete  
	 */
	private String btnDelete;
	/**
	 * is use at the property css display for the button cancel
	 */
	private String btnCancel;
	/**
	 * is use at the property css display for the button save
	 */
	private String btnSave;
	/**
	 * is the string of the title to shown on the page
	 */
	private String title;
	
	/**
	 * is the action to perform when the controller is called 
	 */
	private int actionForm;
	
	/**
	 * default constructor
	 */
	public LocationView() {
		title = "LOCATIONS";
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}

	/**
	 * Constructor with the list to show pre-loaded 
	 * @param list
	 */
	public LocationView(List<LocationForm> list){
		title = "LOCATIONS";
		this.locations = list;
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}
	
	/**
	 * Cosntructor with the list to show pre-loaded and the main form filled with an existing location
	 * @param list
	 * @param location
	 */
	public LocationView(List<LocationForm> list, LocationForm location){
		title = "LOCATIONS";
		this.locations = list;
		this.location = location;
		btnDelete = "display";
		btnCancel = "display";
		btnSave = "display";
	}
	
	/**
	 * Get the string for the property css display for the button Save
	 * @return display || none
	 */
	public String getBtnSave() {
		return btnSave;
	}

	/**
	 * Set the string for the property css display for the button save
	 * @param btnSave [display || none]
	 */
	public void setBtnSave(String btnSave) {
		this.btnSave = btnSave;
	}

	/**
	 * get id of the location object than you want to search in the Database
	 * @return id 
	 */
	public int getFromSearch() {
		return fromSearch;
	}

	/**
	 * Set the id of the location that you want to search in the Database
	 * @param fromSearch
	 */
	public void setFromSearch(int fromSearch) {
		this.fromSearch = fromSearch;
	}

	/**
	 * Get the object than contain all the fields from the main form
	 * @return LocationForm
	 */
	public LocationForm getLocation() {
		return location;
	}

	/**
	 * Set the object that contain all the fields from the main form
	 * @param location
	 */
	public void setLocation(LocationForm location) {
		this.location = location;
	}

	/**
	 * Get the list that contain all the locations than are enable to be use
	 * @return Location List
	 */
	public List<LocationForm> getLocations() {
		return locations;
	}

	/**
	 * Set The list that contain all the location than are enable to be use
	 * @param locations
	 */
	public void setLocations(List<LocationForm> locations) {
		this.locations = locations;
	}

	/**
	 * Get the string for the property css display for the button delete
	 * @return display || none
	 */
	
	public String getBtnDelete() {
		return btnDelete;
	}

	/**
	 * Set the string for the property css display for the button delete
	 * @param btnSave [display || none]
	 */
	public void setBtnDelete(String btnDelete) {
		this.btnDelete = btnDelete;
	}

	/**
	 * Get the string for the property css display for the button cancel
	 * @return display || none
	 */
	public String getBtnCancel() {
		return btnCancel;
	}

	/**
	 * Set the string for the property css display for the button cancel
	 * @param btnSave [display || none]
	 */
	public void setBtnCancel(String btnCancel) {
		this.btnCancel = btnCancel;
	}

	/**
	 * Get the title is being use to display at the page
	 * @return Title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title is being use to display at the page
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Get the action to be perform at the controller
	 * @return Action number
	 */
	public int getActionForm() {
		return actionForm;
	}

	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Set the action to be perfom at the controller
	 * @param actionForm
	 */
	public void setActionForm(int actionForm) {
		this.actionForm = actionForm;
	}
	
	
}