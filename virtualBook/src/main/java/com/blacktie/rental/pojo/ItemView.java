package com.blacktie.rental.pojo;

import java.util.List;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class ItemView {

	/**
	 * is the ID of the location you want to show on the form
	 */
	private int fromSearch;
	/**
	 * is the object for the main form 
	 */
	private ItemForm item;
	/**
	 * contain a list of all the items enable to work with 
	 */
	private List<ItemForm> items;
	/**
	 * is use at the property css display for the button delete  
	 */
	private String btnDelete;
	/**
	 * is use at the property css display for the button cancel
	 */
	private String btnCancel;
	/**
	 * is use at the property css display for the button save
	 */
	private String btnSave;
	/**
	 * is the string of the title to shown on the page
	 */
	private String title;
	/**
	 * is the action to perform when the controller is called 
	 */
	private int actionForm;
	
	/**
	 * Default constructor
	 */
	public ItemView() {
		title = "ITEMS";
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}
	
	/**
	 * Constructor with preloaded list 
	 * @param list
	 */
	public ItemView(List<ItemForm> list){
		title = "ITEMS";
		this.items = list;
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}
	
	/**
	 * Constructor with preloaded list and a pre fill form with an existing item
	 * @param list
	 * @param item
	 */
	public ItemView(List<ItemForm> list, ItemForm item){
		title = "ITEMS";
		this.items = list;
		this.item = item;
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}

	/**
	 * Get id form preselected item form datagrid
	 * @return item id
	 */
	public int getFromSearch() {
		return fromSearch;
	}
	/**
	 * Set id for item to search 
	 * @param fromSearch
	 */
	public void setFromSearch(int fromSearch) {
		this.fromSearch = fromSearch;
	}

	/**
	 * Get item
	 * @return item
	 */
	public ItemForm getItem() {
		return item;
	}

	/**
	 * Set Item
	 * @param item
	 */
	public void setItem(ItemForm item) {
		this.item = item;
	}
	
	/**
	 * Get Items
	 * @return items
	 */
	public List<ItemForm> getItems() {
		return items;
	}
	/**
	 * Set Items
	 * @param items
	 */
	public void setItems(List<ItemForm> items) {
		this.items = items;
	}
	
	/**
	 * Get the string for the property css display for the button delete
	 * @return display || none
	 */
	
	public String getBtnDelete() {
		return btnDelete;
	}

	/**
	 * Set the string for the property css display for the button delete
	 * @param btnSave [display || none]
	 */
	public void setBtnDelete(String btnDelete) {
		this.btnDelete = btnDelete;
	}
	/**
	 * Get the string for the property css display for the button cancel
	 * @return display || none
	 */
	public String getBtnCancel() {
		return btnCancel;
	}
	/**
	 * Set the string for the property css display for the button cancel
	 * @param btnSave [display || none]
	 */
	public void setBtnCancel(String btnCancel) {
		this.btnCancel = btnCancel;
	}

	/**
	 * Get the string for the property css display for the button Save
	 * @return display || none
	 */
	public String getBtnSave() {
		return btnSave;
	}

	/**
	 * Set the string for the property css display for the button save
	 * @param btnSave [display || none]
	 */
	public void setBtnSave(String btnSave) {
		this.btnSave = btnSave;
	}
	/**
	 * Get the title is being use to display at the page
	 * @return Title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * Set the title is being use to display at the page
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Get the action to be perform at the controller
	 * @return Action number
	 */
	public int getActionForm() {
		return actionForm;
	}

	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Set the action to be perfom at the controller
	 * @param actionForm
	 */
	public void setActionForm(int actionForm) {
		this.actionForm = actionForm;
	}
	
	
}
