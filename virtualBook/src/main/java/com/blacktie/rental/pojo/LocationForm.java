package com.blacktie.rental.pojo;

import com.blacktie.rental.configuration.VirtualBook;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class LocationForm {
	/**
	 * Location Id
	 */
	private int id;
	/**
	 * Location name
	 */
	private String name;
	/**
	 * Location address
	 */
	private String address;
	/**
	 * Location Phone number
	 */
	private String phoneNumber;
	/**
	 * Location state
	 */
	private int state;
	/**
	 * Location status
	 */
	private String status;

	/**
	 * Get the Location id
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Set the location id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Get the location name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Set the location name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get the location address
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * Set the location address
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * Get the Location phone number
	 * @return phone number 
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * Set the location phone number
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * 0) disable
	 * 1) enable
	 * Get the location state
 	 * @return state number
	 */
	public int getState() {
		return state;
	}
	/**
	 * 0) disable
	 * 1) enable
	 * Set the location state
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * Get String for the datagrid at the page, the status is apply to the row
	 * @return Disable || Enable
	 */
	public String getStatus() {
		if(state==0){
			return VirtualBook.DISABLE;
		}else{
			return VirtualBook.ENABLE;
		}
	}
	/**
	 * This is an useless method because the value on getStatus is based on the variable STATE.
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
