package com.blacktie.rental.pojo;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class EventForm {

	/**
	 * Event Date
	 */
	private Date date;
	/**
	 * Event Client
	 */
	private ClientView client;
	/**
	 * Event Location
	 */
	private LocationForm location;
	/**
	 * Event Contact 
	 */
	private ClientView contact;
	/**
	 * Event item
	 */
	private ItemForm item;
	
	/**
	 * Event Items List
	 */
	private List<ItemForm> itemsEvent;
	/**
	 * Event Notes
	 */
	private String notes;
	/**
	 * Event SubTotal
	 */
	private Double subTotal;
	/**
	 * Event Delivery cost
	 */
	private Double delivery;
	/**
	 * Event Forward Payment
	 */
	private Double forwardPayment;
	/**
	 * Event taxes 
	 */
	private Double taxes;
	/**
	 * Event total cost
	 */
	private Double total;
	
	/**
	 * this method is use only for test purpose, 
	 * and print in the console the values for the event
	 */
	public void print(){
		System.out.println("Date: "+date);
		if( client != null){
			System.out.println("Client: "+client.getId());
			System.out.println("Client: "+client.getName());
		}
		if(location != null){

			System.out.println("Location "+location.getName());
		}
		if(contact != null){
			System.out.println("Contact: "+contact.getName()+" "+contact.getPhoneNumber());
		}
		if(itemsEvent!=null){

			System.out.println("Items: "+itemsEvent.size());
		}
		System.out.println("Notes: "+notes);
		System.out.println("Subtotal: "+getSubTotal());
		System.out.println("Deliery: "+delivery);
		System.out.println("ForwardPayment: "+forwardPayment);
		System.out.println("taxrs: "+taxes);
		System.out.println("total: "+getTotal());
	}
	
	/**
	 * Get the subtotal of the event, that is calculate:
	 * item count times unite price, with each item from the list
	 * @return subtotal
	 */
	public Double getSubTotal() {
		subTotal = Double.valueOf("0");		
		if(itemsEvent!=null && !itemsEvent.isEmpty()){
			for(ItemForm i: itemsEvent){
				subTotal += i.getUnitPrice()*i.getCount();
			}
		}
		return subTotal;
	}

	/**
	 * Set the event subtotal
	 * @param subTotal
	 */
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * Get the event delivery cost
	 * @return delivery cost
	 */
	public Double getDelivery() {
		return delivery;
	}

	/**
	 * Set Event delivery cost
	 * @param delivery
	 */
	public void setDelivery(Double delivery) {
		this.delivery = delivery;
	}

	/**
	 * Get Event forwardPayment
	 * @return forwardPayment
	 */
	public Double getForwardPayment() {
		return forwardPayment;
	}

	/**
	 * Set Event forwardpayment
	 * @param forwardPayment
	 */
	public void setForwardPayment(Double forwardPayment) {
		this.forwardPayment = forwardPayment;
	}

	/**
	 * Get Event taxes, 
	 * this field is the % that will apply.
	 * @return taxes
	 */
	public Double getTaxes() {
		return taxes;
	}

	/**
	 * Set Event taxes, in % value
	 * @param taxes
	 */
	public void setTaxes(Double taxes) {
		this.taxes = taxes;
	}

	/**
	 * Get event total cost,
	 * is calculate adding the subtotal with delivery cost, plus the taxes, 
	 * and if there is any forwardpayment subtract from the total
	 * @return total
	 */
	public Double getTotal() {
		total = Double.valueOf("0");
		if(subTotal !=null && delivery !=null){
			total = subTotal + delivery;
			if (taxes>0){
				total = (taxes*100)/total;
			}
			return forwardPayment!=null?total-forwardPayment:total;
		}
		return total;
	}

	/**
	 * Set event total cost
	 * @param total
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * Get Event Notes
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Set Event notes
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * Get Event Item
	 * @return item
	 */
	public ItemForm getItem() {
		return item;
	}

	/**
	 * Set Event Item
	 * @param item
	 */
	public void setItem(ItemForm item) {
		this.item = item;
	}
	
	/**
	 * Get Event Date
	 * @return date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Set event Date
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Get Event Client
	 * @return client
	 */
	public ClientView getClient() {
		return client;
	}

	/**
	 * Set Event Client
	 * @param client
	 */
	public void setClient(ClientView client) {
		this.client = client;
	}

	/**
	 * Get Event Contact 
	 * @return contact
	 */
	public ClientView getContact() {
		return contact;
	}

	/**
	 * Set Event Contact
	 * @param contact
	 */
	public void setContact(ClientView contact) {
		this.contact = contact;
	}
	
	/**
	 * Set Event Contact different than the event client
	 * @param name
	 * @param phone
	 */
	public void setContact(String name, String phone) {
		this.contact = new ClientView(name,phone);
	}

	/**
	 * Get Event Location
	 * @return
	 */
	public LocationForm getLocation() {
		return location;
	}

	/**
	 * Set Event Location
	 * @param location
	 */
	public void setLocation(LocationForm location) {
		this.location = location;
	}

	/**
	 * Get Event Items
	 * @return items
	 */
	public List<ItemForm> getItemsEvent() {
		return itemsEvent;
	}

	/**
	 * Set Event Items
	 * @param itemsEvent
	 */
	public void setItemsEvent(List<ItemForm> itemsEvent) {
		this.itemsEvent = itemsEvent;
	}
	
}
