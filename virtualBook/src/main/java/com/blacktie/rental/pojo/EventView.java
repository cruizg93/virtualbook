package com.blacktie.rental.pojo;

import java.util.List;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class EventView {

	/**
	 * EventView EventForm
	 */
	private EventForm event;
	/**
	 * EventView locations
	 */
	private List<LocationForm> locations;
	/**
	 * EvenView Clients
	 */
	private List<ClientView> clients;
	/**
	 * EventView Item
	 */
	private List<ItemForm> items;
	/**
	 * EventView is the contact person same as client
	 */
	private String isClientContact;
	/**
	 * EventView has this event a physical contract
	 */
	private String hasContract;
	
	private String btnDelete;
	private String btnCancel;
	private String btnSave;
	private int actionForm;
	

	public EventView() {
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
	}

	
	
	public String getIsClientContact() {
		return isClientContact;
	}



	public void setIsClientContact(String isClientContact) {
		this.isClientContact = isClientContact;
	}



	public String getHasContract() {
		return hasContract;
	}



	public void setHasContract(String hasContract) {
		this.hasContract = hasContract;
	}



	public int getActionForm() {
		return actionForm;
	}



	public void setActionForm(int actionForm) {
		this.actionForm = actionForm;
	}



	public List<ItemForm> getItems() {
		return items;
	}



	public void setItems(List<ItemForm> items) {
		this.items = items;
	}



	public String getBtnDelete() {
		return btnDelete;
	}

	public void setBtnDelete(String btnDelete) {
		this.btnDelete = btnDelete;
	}

	public String getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(String btnCancel) {
		this.btnCancel = btnCancel;
	}

	public String getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(String btnSave) {
		this.btnSave = btnSave;
	}

	public List<LocationForm> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationForm> locations) {
		this.locations = locations;
	}

	public List<ClientView> getClients() {
		return clients;
	}

	public void setClients(List<ClientView> clients) {
		this.clients = clients;
	}

	public EventForm getEvent() {
		return event;
	}

	public void setEvent(EventForm event) {
		this.event = event;
	}
	
	
}
