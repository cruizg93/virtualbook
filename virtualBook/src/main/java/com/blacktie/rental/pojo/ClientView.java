package com.blacktie.rental.pojo;

import com.blacktie.rental.configuration.VirtualBook;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class ClientView {
	/**
	 * Client id
	 */
	private int id;
	/**
	 * Client name
	 */
	private String name;
	/**
	 * Client lastname
	 */
	private String lastName;
	/**
	 * Client phoneNumber
	 */
	private String phoneNumber;
	/**
	 * Client email
	 */
	private String email;
	/**
	 * Client company name
	 */
	private String companyName;
	/**
	 * Client controller action to perform
	 */
	private int actionForm;
	/**
	 * Client state
	 */
	private int state;
	/**
	 * Client status
	 */
	private String status;
	
	/**
	 * Default Constructor
	 */
	public ClientView(){
		
	}
	
	/**
	 * Constructor than initialize name and phone
	 * @param name
	 * @param phone
	 */
	public ClientView(String name,String phone){
		this.name = name;
		this.phoneNumber = phone;
	}
	
	/**
	 * 0) disable
	 * 1) enable
	 * Get the event state
 	 * @return state number
	 */
	public int getState() {
		return state;
	}
	/**
	 * 0) disable
	 * 1) enable
	 * Set the event state
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * Get String for the datagrid at the page, the status is apply to the row
	 * @return Disable || Enable
	 */
	public String getStatus() {
		if(state==0){
			return VirtualBook.DISABLE;
		}else{
			return VirtualBook.ENABLE;
		}
	}
	/**
	 * This is an useless method because the value on getStatus is based on the variable STATE.
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Get the action to be perform at the controller
	 * @return Action number
	 */
	public int getActionForm() {
		return actionForm;
	}
	/**
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * Set the action to be perform at the controller
	 * @return Action number
	 */
	public void setActionForm(int actionForm) {
		this.actionForm = actionForm;
	}
	/**
	 * Get Client id
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Set Client id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Get Client name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Set Client name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get Client Last name
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Set Client Last Name
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * Get Client phone number
	 * @return phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * Set Client phone number
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * Get Client e-mail
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Set client e-mail
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Get Client Company name
	 * @return company name
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * Set Client Company name 
	 * @param companyName
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}
