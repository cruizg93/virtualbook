package com.blacktie.rental.pojo;

import java.util.List;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class ClientForm {

	/**
	 * is the ID of the client you want to show on the form
	 */
	private int fromSearch;
	/**
	 * is the object for the main form 
	 */
	private ClientView client;
	/**
	 * contain a list of all the Clients enable to work with 
	 */
	private List<ClientView> clients;
	/**
	 * is use at the property css display for the button save
	 */
	private String btnSave;
	/**
	 * is use at the property css display for the button delete  
	 */
	private String btnDelete;
	/**
	 * is use at the property css display for the button cancel
	 */
	private String btnCancel;
	/**
	 * is the string of the title to shown on the page
	 */
	private String title;

	/**
	 * Default constructor
	 */
	public ClientForm() {
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
		title = "CLIENTS";
	}
	
	/**
	 * Constructor with preloaded list 
	 * @param list
	 */
	public ClientForm(List<ClientView> list){
		this.clients = list;
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
		title = "CLIENTS";
	}
	
	/**
	 * Constructor with preloaded list and a pre fill form with an existing client
	 * @param list
	 * @param item
	 */
	public ClientForm(List<ClientView> list, ClientView client){
		this.clients = list;
		this.client = client;
		btnDelete = "none";
		btnCancel = "none";
		btnSave = "display";
		title = "CLIENTS";
	}
	/**
	 * Get the string for the property css display for the button Save
	 * @return display || none
	 */
	public String getBtnSave() {
		return btnSave;
	}
	/**
	 * Set the string for the property css display for the button save
	 * @param btnSave [display || none]
	 */
	public void setBtnSave(String btnSave) {
		this.btnSave = btnSave;
	}
	/**
	 * Get the title is being use to display at the page
	 * @return Title
	 */
	public String getTitle() {
		return "CLIENT";
	}
	/**
	 * Set the title is being use to display at the page
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * Get the string for the property css display for the button cancel
	 * @return display || none
	 */
	public String getBtnCancel() {
		return btnCancel;
	}
	/**
	 * Set the string for the property css display for the button cancel
	 * @param btnSave [display || none]
	 */
	public void setBtnCancel(String btnCancel) {
		this.btnCancel = btnCancel;
	}
	/**
	 * Get the string for the property css display for the button delete
	 * @return display || none
	 */	
	public String getBtnDelete() {
		return btnDelete;
	}
	/**
	 * Set the string for the property css display for the button delete
	 * @param btnSave [display || none]
	 */
	public void setBtnDelete(String btnDelete) {
		this.btnDelete = btnDelete;
	}
	/**
	 * Get id form preselected item form datagrid
	 * @return item id
	 */
	public int getFromSearch() {
		return fromSearch;
	}
	/**
	 * Set id for item to search 
	 * @param fromSearch
	 */
	public void setFromSearch(int fromSearch) {
		this.fromSearch = fromSearch;
	}
	/**
	 * Get client
	 * @return client
	 */
	public ClientView getClient() {
		return client;
	}

	/**
	 * Set client
	 * @param client
	 */
	public void setClient(ClientView client) {
		this.client = client;
	}

	/**
	 * Get Clients
	 * @return clients
	 */
	public List<ClientView> getClients() {
		return clients;
	}

	/**
	 * Set Clients
	 * @param clients
	 */
	public void setClients(List<ClientView> clients) {
		this.clients = clients;
	}

	
}
