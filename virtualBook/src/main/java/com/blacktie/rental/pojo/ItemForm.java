package com.blacktie.rental.pojo;

import com.blacktie.rental.configuration.VirtualBook;

/**
 * 
 * @author Cristian Ruiz
 *
 */
public class ItemForm {

	/**
	 * Item id
	 */
	private int id;
	/**
	 * Item Description
	 */
	private String description;
	/**
	 * Item state
	 */
	private int state;
	/**
	 * Item Status
	 */
	private String status;
	/**
	 * Item count
	 */
	private int count;
	/**
	 * Item unitPrice
	 */
	private Double unitPrice;
	
	
	/**
	 * Get the individual price for the item 
	 * @return unit price
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}
	/**
	 * Set the individual price for the item
	 * @param unitPrice
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * Get how many items it will be rental with this description
	 * @return count
	 */
	public int getCount() {
		return count;
	}
	 /**
	  * Set how many items it will be rental with this description
	  * @param count
	  */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * Get the item id
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * Set the item id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Get the item description
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Set the item description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * 0) disable
	 * 1) enable
	 * Get the Item state
 	 * @return state number
	 */
	public int getState() {
		return state;
	}
	/**
	 * 0) disable
	 * 1) enable
	 * Set the item state
	 * @param state number
	 */
	public void setState(int state) {
		this.state = state;
	}
	/**
	 * Get String for the datagrid at the page, the status is apply to the row
	 * @return Disable || Enable
	 */
	public String getStatus() {
		if(state==0){
			return VirtualBook.DISABLE;
		}else{
			return VirtualBook.ENABLE;
		}
	}
	 
	 /**
	 * This is an useless method because the value on getStatus is based on the variable STATE.
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
}
