package com.blacktie.rental.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.blacktie.rental.model.User;

/**
 * 
 * @author Cristian Ruiz
 *
 */
@Controller
@RequestMapping("user")
public class UserController {

	/**
	 * Default constructor, is called when user.jsp is rendering
	 * @param locale
	 * @return ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showClient(Locale locale){
		return new ModelAndView("user","user",new User());
		
	} 
}