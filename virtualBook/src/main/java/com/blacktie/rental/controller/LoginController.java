package com.blacktie.rental.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blacktie.rental.model.Login;
import com.blacktie.rental.pojo.UserView;
import com.blacktie.rental.service.UserService;
import com.blacktie.rental.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page index.jsp or default page
 */
@Controller
@RequestMapping("/")
public class LoginController {
	private final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	/**
	 * Default method than is called when the page is rendering
	 * @param locale
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLogin(Locale locale){
		logger.info("Welcome valentina! Login The client locale is {}.", locale);
		return new ModelAndView("/","loginForm", new Login());
	}
	
	/**
	 * @param locationView
	 * @param result
	 * @param model
	 * 
	 * This method is called by the form to execute an action through the variable [ActionForm]
	 * Valid actions: 
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * default) list
	 * @return ModelAndView
	 */
	@RequestMapping("/loginNew")
	public ModelAndView addLogin(@ModelAttribute("loginForm") Login login, 
			BindingResult result,Model model){
		try {
			System.out.println("****"+login.getAction());
			UserService userService = new UserServiceImpl();
			UserView user = new UserView();
			user.setUser(login.getUsuario());
			user.setPassword(login.getPassword());
			user = userService.login(user);
			if(user!=null){
				return new ModelAndView("welcome");
			}else{
				model.addAttribute("msgError","That password or username are incorrect");
				return new ModelAndView("/", "loginForm", login);
			}
		} catch (Exception e) {
			System.out.println("-*********************"+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
}
