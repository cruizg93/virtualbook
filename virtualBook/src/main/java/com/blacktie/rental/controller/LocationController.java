package com.blacktie.rental.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blacktie.rental.configuration.VirtualBook;
import com.blacktie.rental.pojo.LocationForm;
import com.blacktie.rental.pojo.LocationView;
import com.blacktie.rental.service.LocationService;
import com.blacktie.rental.service.LocationServiceImpl;

/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page location.jsp
 */
@Controller
@RequestMapping("/location")
public class LocationController {
	
	/**
	 * Default method than is called when the page is rendering
	 * @param locale
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLocation(Locale locale){
		return new ModelAndView("location","locationView",new LocationView(list()));
		
	}
	
	/**
	 * @param locationView
	 * @param result
	 * @param model
	 * 
	 * This method is called by the form to execute an action through the variable [ActionForm]
	 * Valid actions: 
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * default) list
	 * @return ModelAndView
	 */
	@RequestMapping(value="/manageLocation", method = RequestMethod.POST)
	public ModelAndView manageLocation(@ModelAttribute("locationView") LocationView locationView,
			BindingResult result, Model model){
	
		List<LocationForm> list = null;
		LocationForm loc = locationView.getLocation();
		LocationForm locResult = null;
		/***
		 * 
		 * recuerda organizar el estado al editar
		 */
		try {
			LocationService service = new LocationServiceImpl();
			
			switch (locationView.getActionForm()) {
			case 1:
				/**** edit begin ****/
				/**** if the field search has an Id proceed to edit if no, save a new location ***/
				if(locationView.getFromSearch()>0){
					loc.setId(locationView.getFromSearch());
					
					locResult = service.update(loc);
					if(locResult!=null){
						if(locResult.getId()>0){
							model.addAttribute("msg","Edit-Succes!");
							return new ModelAndView("location","locationView",new LocationView(list()));
						}else{
							model.addAttribute("msg","Error editing this location");
							return new ModelAndView("location","locationView",locationView);
						}
					}
					break;
					
				}
				/**** edit end****/
				locResult = service.save(loc);
				if(locResult == null){
					model.addAttribute("msg","Existing location with the same Address or name");
					return new ModelAndView("location","locationView",locationView);
				}
				if(locResult.getId()>0){
					model.addAttribute("msg","Save-Succes!");
					return new ModelAndView("location","locationView",new LocationView(list()));
				}else{
					model.addAttribute("msg","Existing user with the same Address or name");
					return new ModelAndView("location","locationView",locationView);
				}
			case 2:
				model.addAttribute("msg","Listar...");
				return new ModelAndView("location","locationView", new LocationView(list()));
			case 3:
				service.delete(loc);
				model.addAttribute("msg","Delete-Success!");
				return new ModelAndView("location","locationView",new LocationView(list()));
			case 4:
				loc.setId(locationView.getFromSearch());
				locResult = service.search(loc);
				if(locResult==null){
					model.addAttribute("msg","Error");
					return new ModelAndView("location","locationView",new LocationView(list()));
				}
				LocationView lc = new LocationView();
				lc.setLocation(locResult);
				lc.setLocations(list());
				lc.setFromSearch(locationView.getFromSearch());
				
				if(lc.getLocation().getStatus().equals(VirtualBook.DISABLE)){
					lc.setBtnSave("none");
					lc.setBtnCancel("display");
					lc.setBtnDelete("none");
				}else{
					lc.setBtnSave("display");
					lc.setBtnDelete("display");
					lc.setBtnCancel("display");
				}
				return new ModelAndView("location","locationView", lc);
			default:
				locationView.setLocations(list());
				return new ModelAndView("location","locationView",new LocationView(list()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * This method call list form the service to search all the location with state enable.
	 * @return Location Form list
	 */
	private List<LocationForm>  list(){
		List<LocationForm> l = null;
		LocationService service = new LocationServiceImpl();
		try {
			l = service.list();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return l;
	}
}