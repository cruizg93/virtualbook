package com.blacktie.rental.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page welcome.jsp
 */
@Controller
@RequestMapping("welcome")
public class WelcomeController {
	
private final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	/**
	 * Default method, is called when welcome.jsp is rendering
	 * @param locale
	 * @return ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showClient(Locale locale){
		return new ModelAndView("welcome","welcome",new Object());
		
	} 

}
