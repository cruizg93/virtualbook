package com.blacktie.rental.controller;

import java.util.List;
import java.util.Locale;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blacktie.rental.configuration.VirtualBook;
import com.blacktie.rental.pojo.ClientForm;
import com.blacktie.rental.pojo.ClientView;
import com.blacktie.rental.service.ClientService;
import com.blacktie.rental.service.ClientServiceImpl;

/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page client.jsp
 */

@Controller
@RequestMapping("/client")
public class ClientController {

	private final Logger logger = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	ServletContext context;
	
	/**
	 * Default method than is called when the page is rendering
	 * @param locale
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showClient(Locale locale){
		logger.info("Welcome! Valentina - client The Client locale is {}.",locale);
		ClientService service = new ClientServiceImpl();
		List list =service.list();
		return new ModelAndView("client","clientForm", new ClientForm(list));
	} 
	
	
	/**
	 * @param ClientView
	 * @param result
	 * @param model
	 * 
	 * This method is called by the form to execute an action through the variable [ActionForm]
	 * Valid actions: 
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * default) list
	 * @return ModelAndView
	 */
	@RequestMapping(value="/manageClient", method = RequestMethod.POST)
	public ModelAndView saveClient(@ModelAttribute("clientForm") ClientForm clientForm,
			BindingResult result, Model model){
		
		System.out.println("*********************************************************************************"+context.getContextPath());	
		List<ClientView> list = null;
		ClientView client = clientForm.getClient();
		ClientView clientResult = null;
		
		try {
			ClientService service = new ClientServiceImpl();
			switch (client.getActionForm()) {
			case 1:
				/****edit begin***/
				if(clientForm.getFromSearch()>0){
					client.setId(clientForm.getFromSearch());
					clientResult = service.update(client);
					if(clientResult.getId() > 0 ){
						model.addAttribute("msg","Edit-Succes!");
						return new ModelAndView("client","clientForm",new ClientForm(list()));
					}else{
						model.addAttribute("msg","Error editing this client");
						return new ModelAndView("client","clientForm",clientForm);
					}
				}
				/****edit end ****/
				clientResult = service.save(client);
				if(clientResult == null){
					model.addAttribute("msg","Existing user with the same Email or Phone Number");
					return new ModelAndView("client","clientForm",clientForm);
				}
				if(clientResult.getId() > 0 ){
					model.addAttribute("msg","Save-Succes!");
				}else{
					model.addAttribute("msg","Existing user with the same Email or Phone Number");
					return new ModelAndView("client","clientForm",clientForm);
				}
				break;
			case 2:
				model.addAttribute("msg","Listar...");
				return new ModelAndView("client","clientForm", new ClientForm(list()));
			case 3:
				service.delete(client);
				model.addAttribute("msg","Edit-Succes!");
				return new ModelAndView("client","clientForm",new ClientForm(list()));
			case 4:
				client.setId(clientForm.getFromSearch());
				clientResult = service.search(client);
				if(clientResult==null){
					model.addAttribute("msg","Error");
					return new ModelAndView("item","itemView",new ClientForm(list()));
				}
				ClientForm cf = new ClientForm();
				cf.setClient(clientResult);
				cf.setClients(list());
				cf.setFromSearch(clientForm.getFromSearch());
				
				if(cf.getClient().getStatus().equals(VirtualBook.DISABLE)){
					cf.setBtnSave("none");
					cf.setBtnCancel("display");
					cf.setBtnDelete("none");
				}else{
					cf.setBtnSave("display");
					cf.setBtnDelete("display");
					cf.setBtnCancel("display");
				}
				return new ModelAndView("client","clientForm", cf);
			default:
				clientForm.setClients(list());
				return new ModelAndView("client", "clientForm", clientForm);
			}
			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return new ModelAndView("client","clientForm",new ClientForm(list()));
	}

	/**
	 * This method call list form the service to search all the location with state enable.
	 * @return Location Form list
	 */
	private List<ClientView>  list(){
		List<ClientView> l = null;
		ClientService service = new ClientServiceImpl();
		try {
			l = service.list();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return l;
	}
}
