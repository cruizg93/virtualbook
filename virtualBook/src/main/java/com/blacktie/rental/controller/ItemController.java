package com.blacktie.rental.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blacktie.rental.configuration.VirtualBook;
import com.blacktie.rental.pojo.ItemForm;
import com.blacktie.rental.pojo.ItemView;
import com.blacktie.rental.service.ItemService;
import com.blacktie.rental.service.ItemServiceImpl;


/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page item.jsp
 */
@Controller
@RequestMapping("/item")
public class ItemController {

	/**
	 * Default method than is called when the page is rendering
	 * @param locale
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showClient(Locale locale){
		return new ModelAndView("item","itemView",new ItemView(list()));
	}

	/**
	 * @param ItemView
	 * @param result
	 * @param model
	 * 
	 * This method is called by the form to execute an action through the variable [ActionForm]
	 * Valid actions: 
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * default) list
	 * @return ModelAndView
	 */
	@RequestMapping(value="/manageItem", method = RequestMethod.POST)
	public ModelAndView manageLocation(@ModelAttribute("itemView") ItemView itemView,
			BindingResult result, Model model){
	
		List<ItemForm> list = null;
		ItemForm loc = itemView.getItem();
		ItemForm locResult = null;
		/***
		 * 
		 * recuerda organizar el estado al editar
		 */
		try {
			ItemService service = new ItemServiceImpl();
			
			switch (itemView.getActionForm()) {
			case 1:
				/**** edit begin ****/
				/**** if the field search has an Id proceed to edit if no, save a new location ***/
				
				if(itemView.getFromSearch()>0){
					loc.setId(itemView.getFromSearch());
					
					locResult = service.update(loc);
					if(locResult!=null){
						if(locResult.getId()>0){
							model.addAttribute("msg","Edit-Succes!");
							return new ModelAndView("item","itemView",new ItemView(list()));
						}else{
							model.addAttribute("msg","Error editing this item");
							return new ModelAndView("item","itemView",itemView);
						}
					}
					break;
					
				}
				/**** edit end****/
				locResult = service.save(loc);
				if(locResult == null){
					model.addAttribute("msg","Existing Item with the same Description");
					return new ModelAndView("item","itemView",itemView);
				}
				if(locResult.getId()>0){
					model.addAttribute("msg","Save-Succes!");
					return new ModelAndView("item","itemView",new ItemView(list()));
				}else{
					model.addAttribute("msg","Existing Item with the same Description");
					return new ModelAndView("item","itemView",itemView);
				}
			case 2:
				model.addAttribute("msg","Listar...");
				return new ModelAndView("item","itemView", new ItemView(list()));
			case 3:
				service.delete(loc);
				model.addAttribute("msg","Delete-Success!");
				return new ModelAndView("item","itemView",new ItemView(list()));
			case 4:
				
				loc.setId(itemView.getFromSearch());
				locResult = service.search(loc);
				if(locResult==null){
					model.addAttribute("msg","Error");
					return new ModelAndView("item","itemView",new ItemView(list()));
				}
				ItemView lc = new ItemView();
				lc.setItem(locResult);
				lc.setItems(list());
				lc.setFromSearch(itemView.getFromSearch());
				
				if(lc.getItem().getStatus().equals(VirtualBook.DISABLE)){
					lc.setBtnSave("none");
					lc.setBtnCancel("display");
					lc.setBtnDelete("none");
				}else{
					lc.setBtnSave("display");
					lc.setBtnDelete("display");
					lc.setBtnCancel("display");
				}
				return new ModelAndView("item","itemView", lc);
			default:
				itemView.setItems(list());
				return new ModelAndView("item","itemView",new ItemView(list()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	/**
	 * This method call list form the service to search all the location with state enable.
	 * @return Location Form list
	 */
	private List<ItemForm>  list(){
		List<ItemForm> l = null;
		ItemService service = new ItemServiceImpl();
		try {
			l = service.list();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return l;
	}
}