package com.blacktie.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.blacktie.rental.pojo.EventView;
import com.blacktie.rental.service.EventService;
import com.blacktie.rental.service.EventServiceImpl;

import java.util.Locale;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Cristian Ruiz
 * this controller is attach to the page Event.jsp
 */
@Controller
@RequestMapping("/event")
public class EventController {

	
	@Autowired
	ServletContext context;

	EventService service;
	/**
	 * Default method than is called when the page is rendering
	 * @param locale
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showEvent(Locale locale){
		service = new EventServiceImpl();
		EventView view = new EventView();
		view = service.loadConfig(view);
		return new ModelAndView("event","eventView",view);
	}
	
	/**
	 * @param EventView
	 * @param result
	 * @param model
	 * 
	 * This method is called by the form to execute an action through the variable [ActionForm]
	 * Valid actions: 
	 * 1)save
	 * 2)list
	 * 3)delete
	 * 4)search
	 * default) list
	 * @return ModelAndView
	 */
	@RequestMapping(value="/manageEvent", method = RequestMethod.POST)
	public ModelAndView manageEvent(@ModelAttribute("eventView") EventView eventView,
			BindingResult result, Model model){
		
		EventService service = new EventServiceImpl();
		
		switch (eventView.getActionForm()) {
		case 1:
			eventView = service.save(eventView);
			break;
		case 2:
			 
			break;
		default:
			break;
		}
		return new ModelAndView("event","eventView",new EventView());
	}
	
}
