package com.blacktie.rental.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Cristian Ruiz
 * This entitiy is for the table tbl_users
 */

@Entity
@Table(name= "tbl_users")
public class User {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "user")
	private String user;
	
	@Column(name = "password")
	private String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
