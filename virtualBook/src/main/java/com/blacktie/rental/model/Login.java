package com.blacktie.rental.model;

/**
 * 
 * @author Cristian Ruiz 
 *
 */
public class Login {
	/**
	 * Login usuario
	 */
	private String usuario;
	/**
	 * Login password
	 */
	private String password;
	/**
	 * Login action
	 */
	private int action;
	/**
	 * Login msg
	 */
	private String msg;
	/**
	 * Login title
	 */
	private String title;

	/**
	 * Default constructor
	 */
	public Login(){
		title="LOGIN";
	}
	
	/**
	 * Get the title that is be showing at the page
	 * @return title
	 */
	public String getTitle() {
		return "LOGIN";
	}

	/**
	 * Set the title to be showing at the page
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get the login user 
	 * @return user
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set Login user
	 * @param usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Get login message
	 * @return message
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Set login message
	 * @param msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Get login password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Set Login Password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Get Login action
	 * @return action
	 */
	public int getAction() {
		return action;
	}

	/**
	 * Set Login Action
	 * @param action
	 */
	public void setAction(int action) {
		this.action = action;
	}
	
	
}
