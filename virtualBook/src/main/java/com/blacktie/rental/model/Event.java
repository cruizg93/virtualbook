package com.blacktie.rental.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author Cristian Ruiz
 * Entity for table tbl_events
 */

@Entity
@Table(name="tbl_events")
public class Event {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name="date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private Location location;
	
	@ManyToOne
	@JoinColumn(name="client_id")
	private Client client;
	
	@Column(name="contact_name")
	private String contactName;
	
	@Column(name="contact_phone")
	private String contactPhone;
	
	@Column(name="contract")
	private Date contract;
	
	@Column(name="notes")
	private String notes;
	
	@Column(name="forward_payment")
	private Double forwardPayment;
	
	@Column(name="taxes")
	private Double taxes;
	
	@Column(name="delivery")
	private Double delivery;
	
	@Column(name="contact_same_client")
	private int isContactClient;

	@OneToMany(mappedBy="itemsEvent")
	private Set<Item> items;
	
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public Date getContract() {
		return contract;
	}

	public void setContract(Date contract) {
		this.contract = contract;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Double getForwardPayment() {
		return forwardPayment;
	}

	public void setForwardPayment(Double forwardPayment) {
		this.forwardPayment = forwardPayment;
	}

	public Double getTaxes() {
		return taxes;
	}

	public void setTaxes(Double taxes) {
		this.taxes = taxes;
	}

	public Double getDelivery() {
		return delivery;
	}

	public void setDelivery(Double delivery) {
		this.delivery = delivery;
	}

	public int getIsContactClient() {
		return isContactClient;
	}

	public void setIsContactClient(int isContactClient) {
		this.isContactClient = isContactClient;
	}
}