package com.blacktie.rental.configuration;

/**
 * 
 * @author Cristian Ruiz
 * This class is used to declare static/final values 
 */
public class VirtualBook {

	
	public static final String ENABLE = "enabled";
	public static final String DISABLE = "disabled";
}
